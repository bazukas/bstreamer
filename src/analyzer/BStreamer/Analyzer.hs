module BStreamer.Analyzer
( mainLoop
, processAnalyzeTask
) where

import Control.Concurrent (threadDelay)
import Control.Monad (forever, replicateM_)
import Control.Monad.Trans.Class (lift)
import Control.Monad.Trans.Reader (ask, asks, runReaderT)
import Data.Time.Clock (UTCTime)
import qualified Data.Text as T

import BStreamer.AMQP
import BStreamer.Analyzer.Env
import BStreamer.Analyzer.EventData
import BStreamer.Analyzer.GraphData
import BStreamer.Analyzer.Reactions
import BStreamer.Analyzer.WordCloud
import BStreamer.Config
import BStreamer.Log
import BStreamer.Time
import BStreamer.Types.Broadcast
import BStreamer.Types.Broadcast.Data
import BStreamer.Types.ChannelStat
import BStreamer.Types.ChatEvent
import BStreamer.Types.ChatMessage
import BStreamer.Util

logger :: String
logger = "bstreamer.analyzer"

generateBroadcastData :: UTCTime -> UTCTime -> [ChannelStat] -> [ChatMessage] -> [ChatEvent] -> BroadcastData
generateBroadcastData ds de css cms ces = BroadcastData bgdata bedata wcdata rsdata title avgview avgchat followgain
  where bgdata = generateGraphData ds de css cms ces
        bedata = generateEventData css ces
        wcdata = generateWordCloud (roundTimeMinute ds) de cms
        rsdata = generateReactions cms
        title = mostFrequent $ fmap (stStatus . csStat) css
        avgview = average $ fmap gdViewers bgdata
        avgchat = average $ fmap gdChatActivity bgdata
        followgain = last followdata - head followdata
        followdata = fmap gdFollowers bgdata

processAnalyzeTask :: AnalyzeTask -> BSEnvT IO ()
processAnalyzeTask task = do
  dsConn <- asks envDSConn
  lift $ logDebugL logger ["Analyze task with id: ", T.pack $ show task]
  castM <- lift $ getBroadcast task dsConn
  case castM of
    Nothing -> lift $ logWarningL logger ["Could not find broadcast: ", T.pack . show $ task]
    Just cast -> do
      lift $ saveBroadcast (cast { bcStatus = InProgress }) dsConn
      mChatMessages <- lift $ getChatMessages (bcChannel cast) (bcDateStart cast) (bcDateEnd cast) dsConn
      chatEvents <- lift $ getChatEvents (bcChannel cast) (bcDateStart cast) (bcDateEnd cast) dsConn
      case mChatMessages of
        Nothing -> do
          lift $ logWarningL logger ["Could retrieve chat messages for broadcast: ", T.pack . show $ task]
          lift $ saveBroadcast (cast { bcStatus = Error }) dsConn
        Just chatMessages -> do
          let brData = generateBroadcastData (bcDateStart cast) (bcDateEnd cast) (bcStats cast) chatMessages chatEvents
          lift $ saveBroadcast (cast { bcData = Just brData, bcStatus = Success }) dsConn

initConsumer :: BSEnvT IO ()
initConsumer = do
  lift $ logInfo logger "Initializing analyze task consumer"
  env <- ask
  chan <- lift . openAMQPChannel =<< asks envAMQPConn
  lift $ consumeAnalyzeTasks chan (\t -> runReaderT (processAnalyzeTask t) env)

mainLoop :: BSEnvT IO ()
mainLoop = do
  lift $ logInfo logger "Starting main loop"
  consumers <- asks (aConsumersNum . bsAnalyzerConf . envConf)
  replicateM_ consumers initConsumer
  forever $ lift $ threadDelay 100000000
