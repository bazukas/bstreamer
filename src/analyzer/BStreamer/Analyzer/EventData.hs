module BStreamer.Analyzer.EventData
( generateEventData
) where

import Data.List (sort)

import BStreamer.Types.Broadcast.Data
import BStreamer.Types.ChannelStat
import BStreamer.Types.ChatEvent
import BStreamer.Types.StreamEvent
import BStreamer.Util

channelStatToEventData :: [ChannelStat] -> [EventData]
channelStatToEventData (cs:css) = gameEvents ++ statusEvents
  where gameEvents = gp2ed <$> foldl (foldg cs2g) [(cs2g cs, csTimestamp cs)] css
        statusEvents = sp2ed <$> foldl (foldg cs2s) [(cs2s cs, csTimestamp cs)] css
        foldg acc ((g,ts):xs) cs' = if acc cs' /= g then (acc cs', csTimestamp cs') : (g,ts) : xs else (g,ts) : xs
        foldg _ [] _ = []
        cs2g = stGame . csStat
        cs2s = stStatus . csStat
        gp2ed (g,ts) = EventData ts (GameChange g)
        sp2ed (s,ts) = EventData ts (StatusChange s)
channelStatToEventData [] = []

chatEventToEventData :: [ChatEvent] -> [EventData]
chatEventToEventData ces = remDups (filterEvents [SubscriberOnlyOn, SubscriberOnlyOff]
                                 ++ filterEvents [R9KOn, R9KOff]
                                 ++ filterEvents [EmoteOnlyOn, EmoteOnlyOff]
                                 ++ filter (filterSlow . edEvent) allEvents) edEvent
  where
    filterEvents types = filter (\ed -> edEvent ed `elem` types) allEvents
    filterSlow (SlowMode _) = True
    filterSlow SlowModeOff = True
    filterSlow _ = False
    allEvents = fmap (\ce -> EventData (ceTimestamp ce) (ceEvent ce)) ces

generateEventData :: [ChannelStat] -> [ChatEvent] -> [EventData]
generateEventData css ces = sort $ channelStatToEventData css ++ chatEventToEventData ces
