module BStreamer.Analyzer.WordCloud
( generateWordCloud
) where

import Data.Ord (comparing)
import Data.List (sortBy)
import Data.Text (Text)
import Data.Time.Clock (UTCTime, NominalDiffTime, addUTCTime, diffUTCTime)
import qualified Data.Map.Strict as Map

import BStreamer.Analyzer.Tokenizer
import BStreamer.Types.Broadcast.Data
import BStreamer.Types.ChatMessage

periodLength :: NominalDiffTime
periodLength = 60 * 15

wordsPerCloud :: Int
wordsPerCloud = 30

generateTimePeriods :: UTCTime -> UTCTime -> [(UTCTime, UTCTime)]
generateTimePeriods ds de = generate ds
  where generate ts
          | addUTCTime period ts < de = (ts, addUTCTime period ts) : generate (addUTCTime period ts)
          | otherwise = [(ts, de)]
        period = broadcastLength / fromIntegral (ceiling (broadcastLength / periodLength) :: Integer)
        broadcastLength = diffUTCTime de ds

getMessages :: [ChatMessage] -> [(UTCTime, UTCTime)] -> [(UTCTime, UTCTime, [ChatMessage])]
getMessages cms ((ts,te):tps) = let (res1,res2) = getMessages' cms te in (ts, te, res1) : getMessages res2 tps
  where getMessages' (cm:cms') ts'
          | cmTimestamp cm < ts' = let (res1,res2) = getMessages' cms' ts' in (cm:res1,res2)
          | otherwise = ([],cms')
        getMessages' [] _ = ([],[])
getMessages _ [] = []

countWords :: [Text] -> Map.Map Text Int
countWords texts = count $ concatMap tokenize texts
  where count = foldr (\t m -> Map.insertWith (+) t 1 m) Map.empty

genWordCloud :: (UTCTime, UTCTime, [ChatMessage]) -> WordCloud
genWordCloud (ds, de, cms) = WordCloud ds de (countWords $ fmap cmMessage cms)

filterLowCount :: WordCloud -> WordCloud
filterLowCount (WordCloud ds de wm) = WordCloud ds de filtered
  where filtered = Map.fromList $ take wordsPerCloud $ sortBy (flip $ comparing snd) $ Map.toList wm

generateWordCloud :: UTCTime -> UTCTime -> [ChatMessage] -> [WordCloud]
generateWordCloud ds de cms =
  (filterLowCount . genWordCloud) <$> getMessages cms (generateTimePeriods ds de)
