module BStreamer.Analyzer.Helpers
( uAnalyzeBroadcast
, uGetBroadcast
, uGetBroadcastMessages
) where

import Control.Monad.Trans.Except (runExceptT)
import Control.Monad.Trans.Reader (runReaderT)
import Data.Text (Text)

import BStreamer.Analyzer
import BStreamer.Analyzer.Env
import BStreamer.Types.Broadcast
import BStreamer.Types.ChatMessage

uGetBroadcast :: Text -> IO (Maybe Broadcast)
uGetBroadcast bid = do
  ebsenv <- runExceptT createEnv
  case ebsenv of
    Left _ -> return Nothing
    Right bsenv -> getBroadcast bid (envDSConn bsenv)

uGetBroadcastMessages :: Text -> IO (Maybe [ChatMessage])
uGetBroadcastMessages bid = do
  ebsenv <- runExceptT createEnv
  case ebsenv of
    Left _ -> return Nothing
    Right bsenv -> do
      mBc <- getBroadcast bid (envDSConn bsenv)
      case mBc of
        Nothing -> return Nothing
        Just bc -> getChatMessages (bcChannel bc) (bcDateStart bc) (bcDateEnd bc) (envDSConn bsenv)

uAnalyzeBroadcast :: Text -> IO ()
uAnalyzeBroadcast bid = do
  ebsenv <- runExceptT createEnv
  case ebsenv of
    Left err -> print (show err)
    Right bsenv -> do
      runReaderT (processAnalyzeTask bid) bsenv
      destroyEnv bsenv
