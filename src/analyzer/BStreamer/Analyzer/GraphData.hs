module BStreamer.Analyzer.GraphData
( generateGraphData
) where

import Data.List (foldl')
import Data.Maybe (catMaybes)
import Data.Text (Text)
import Data.Time.Clock (UTCTime, NominalDiffTime, diffUTCTime)
import qualified Data.Map.Strict as Map

import BStreamer.Time
import BStreamer.Types.Broadcast.Data
import BStreamer.Types.ChannelStat
import BStreamer.Types.ChatEvent
import BStreamer.Types.ChatMessage
import BStreamer.Types.StreamEvent
import BStreamer.Util

getStatData :: [ChannelStat] -> UTCTime -> Maybe Stat
getStatData css ts = Map.lookup ts stData
  where stData = Map.map stAvg csMap
        csMap = Map.fromListWith (++) $ fmap (\cs -> (roundTimeMinute (csTimestamp cs), [csStat cs])) css
        stAvg xs = Stat (intAvg $ map stViewers xs)
                        (intAvg $ map stFollowers xs)
                        (stGame $ head xs)
                        (stStatus $ head xs)

getChatData :: [ChatMessage] -> UTCTime -> Int
getChatData cms ts = Map.findWithDefault 0 ts caData
  where caData = Map.map length cmMap
        cmMap = Map.fromListWith (++) $ fmap (\cm -> (roundTimeMinute (cmTimestamp cm), [cm])) cms

getEventData :: [ChatEvent] -> UTCTime -> Int
getEventData ces ts = Map.findWithDefault 0 ts ceData
  where ceData = Map.map length ceMap
        ceMap = Map.fromListWith (++) $ (\ce -> (roundTimeMinute (ceTimestamp ce), [ce])) <$> filter filterCes ces
        filterCes (ChatEvent _ _ ClearChat) = True
        filterCes (ChatEvent _ _ (Ban _ _)) = True
        filterCes (ChatEvent _ _ TimeOut{}) = True
        filterCes _ = False

getLurkerData :: [ChatMessage] -> UTCTime -> Int
getLurkerData cms ts = Map.findWithDefault 0 ts laData
  where laData = Map.map length laMap
        laMap = Map.fromListWith (++) $ fmap (\cm -> (roundTimeMinute (cmTimestamp cm), [cm])) lurkerCms
        lurkerCms = concatMap fst . Map.elems . foldl' lurkerFolder Map.empty $ cms

type FoldContainer = Map.Map Text ([ChatMessage], UTCTime)

lurkerCooldown :: NominalDiffTime
lurkerCooldown = 30 * 60

lurkerFolder :: FoldContainer -> ChatMessage -> FoldContainer
lurkerFolder lmap cm@(ChatMessage _ ts u _ _ _ _) = Map.alter alterF u lmap
  where alterF (Just (cms, lts))
          | diffUTCTime ts lts <= lurkerCooldown = Just (cms, ts)
          | otherwise = Just (cm:cms, ts)
        alterF Nothing = Just ([cm], ts)

generateGraphData :: UTCTime -> UTCTime -> [ChannelStat] -> [ChatMessage] -> [ChatEvent] -> [GraphData]
generateGraphData ds de css cms ces = catMaybes $ fmap makeGraphData timestamps
  where timestamps = generateMinuteSeries ds de
        makeGraphData ts = do
          st <- stLookup ts
          let ca = caLookup ts
          let ma = ceLookup ts
          let la = laLookup ts
          return $ GraphData ts (stViewers st) (stFollowers st) ca ma la
        stLookup = getStatData css
        caLookup = getChatData cms
        ceLookup = getEventData ces
        laLookup = getLurkerData cms
