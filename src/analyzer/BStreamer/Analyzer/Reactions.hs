{-# LANGUAGE TupleSections #-}
module BStreamer.Analyzer.Reactions
( generateReactions
) where

import Control.Arrow ((&&&))
import Data.List (sortOn, foldl')
import Data.Maybe (catMaybes)
import Data.Time.Clock (UTCTime, NominalDiffTime, diffUTCTime)
import qualified Data.Map.Strict as Map
import qualified Data.Text as T

import BStreamer.Analyzer.Tokenizer
import BStreamer.Util
import BStreamer.Types.Broadcast.Data
import BStreamer.Types.ChatMessage

reactionTimeThreshold :: NominalDiffTime
reactionTimeThreshold = 5

reactionSizeThreshold :: Int
reactionSizeThreshold = 3

generateReactions :: [ChatMessage] -> [Reaction]
generateReactions = reverse . sortOn rSize . catMaybes . fmap makeReaction . filter reactionFilter . splitMessages

type PreReaction = ([T.Text], UTCTime)
type FoldContainer = Map.Map T.Text ([PreReaction], UTCTime)

splitMessages :: [ChatMessage] -> [PreReaction]
splitMessages = concatMap fst . Map.elems . foldl' reactionFold Map.empty . prefold
  where prefold = concatMap ((\(msg, ts) -> (,ts) <$> tokenize msg) . (cmMessage &&& cmTimestamp))

reactionFold :: FoldContainer -> (T.Text, UTCTime) -> FoldContainer
reactionFold rmap (msg, ts) = Map.alter alterF (T.toLower msg) rmap
  where alterF (Just ((msgs,fts):prs, lts))
          | diffUTCTime ts lts <= reactionTimeThreshold = Just ((msg:msgs,fts):prs, ts)
          | otherwise = Just (([msg],ts):(msgs,fts):prs, ts)
        alterF (Just ([], _)) = Just ([([msg], ts)], ts)
        alterF Nothing = Just ([([msg], ts)], ts)

reactionFilter :: PreReaction -> Bool
reactionFilter (msgs, _) = (>= reactionSizeThreshold) . length $ msgs

makeReaction :: PreReaction -> Maybe Reaction
makeReaction ([], _) = Nothing
makeReaction (msgs, ts) = Just $ Reaction (mostFrequent msgs) ts (length msgs)
