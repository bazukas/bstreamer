module BStreamer.Analyzer.Env
( BSEnvT
, BSEnv(..)
, createEnv
, destroyEnv
) where

import Control.Monad.Trans.Class (lift)
import Control.Monad.Trans.Except (ExceptT(..))
import Control.Monad.Trans.Reader (ReaderT)

import BStreamer.AMQP
import BStreamer.Config
import BStreamer.DataStorage
import BStreamer.Log
import BStreamer.Types

data BSEnv = BSEnv {
    envConf :: BSettings
  , envDSConn :: DSConn
  , envAMQPConn :: AMQPConn
}

type BSEnvT = ReaderT BSEnv

logger :: String
logger = "bstreamer.analyzer.env"

createEnv :: ExceptT BSError IO BSEnv
createEnv = do
  conf <- lift readSettings
  lift $ logInfo logger "Initializing environment"
  dsConn <- ExceptT $ connectDS (bsDSConf conf)
  amqpConn <- lift $ connectAMQPServer (bsAMQPConf conf)
  return $ BSEnv conf dsConn amqpConn

destroyEnv :: BSEnv -> IO ()
destroyEnv benv = do
  logInfo logger "Terminating environment"
  disconnectDS $ envDSConn benv
  disconnectAMQPServer $ envAMQPConn benv
