module Main where

import Control.Exception as E
import Web.Spock

import BStreamer.Log
import BStreamer.Types
import BStreamer.Web
import BStreamer.Web.Types

logger :: String
logger = "bstreamer.web"

runServer :: BState -> IO ()
runServer state = do
  conf <- appConf state
  runSpockNoBanner 8080 $ spock conf app

main :: IO ()
main = E.bracket acquire release action
  where
    acquire = do
      initializeLoggers "web"
      initState
    release eState = do
      case eState of
        Left _ -> return ()
        Right state -> termState state
      terminateLoggers
    action (Left err) = logError logger err "initializing environment"
    action (Right state) = runServer state `E.catches` [
        E.Handler handleAsync
      , E.Handler handleSome
      ]
    -- uncaught exception handlers
    handleAsync :: E.AsyncException -> IO ()
    handleAsync E.UserInterrupt = E.throwIO E.UserInterrupt
    handleAsync ex = handleUncaught ex
    handleSome :: E.SomeException -> IO ()
    handleSome = handleUncaught
    handleUncaught ex = do
      logError logger (UncaughtError . show $ ex) "running main program"
      E.throwIO ex
