module BStreamer.Web.Views
( landingView
, renderHtml
, topView
, messageView
) where

import Control.Monad (when)
import Data.Maybe (fromMaybe)
import Lucid
import Web.Spock
import qualified Data.Text as T

import BStreamer.Web.Types
import BStreamer.Web.Urls

renderHtml :: Html a -> BSActionCtx ctx ()
renderHtml h = do
  setHeader "Content-Type" "text/html; charset=utf-8"
  lazyBytes . renderBS $ h

topView :: Maybe (Html ()) -> Maybe T.Text -> Bool -> Html () -> Html ()
topView mhead title logout content = do
  doctype_
  html_ $ do
    head_ [lang_ "en"] $ do
      meta_ [charset_ "utf-8"]
      title_ getTitle
      link_ [rel_ "stylesheet", href_ "https://cdnjs.cloudflare.com/ajax/libs/normalize/4.2.0/normalize.min.css"]
      link_ [rel_ "stylesheet", href_ "/static/css/basscss.css"]
      link_ [rel_ "stylesheet", href_ "/static/css/bstreamer.css"]
      fromMaybe (return ()) mhead
    body_ [class_ "line-height-4"] $ do
      div_ [class_ "wrapper"] $ do
        header_ [class_ "clearfix bg-bc3"] $ do
          div_ [class_ "col white"] $
            a_ [href_ (renderRoute root), class_ "btn py2 regular"] "BStreamer"
          div_ [class_ "col-right white"] $ do
            a_ [href_ (renderRoute root), class_ "btn py2 regular"] "Home"
            a_ [href_ (renderRoute dashboardPath), class_ "btn py2 regular"] "Dashboard"
            when logout $
              a_ [href_ "/auth/logout", class_ "btn py2 regular"] "Log Out"
        div_ content
        div_ [class_ "push"] ""
      footer_ [class_ "footer white bg-bc4"] $
        div_ [class_ "pt1"] $
          div_ [class_ "clearfix"] $
            div_ [class_ "col col-right pr1"] $
              a_ [href_ "#", data_ "gsc-widget" "116114", class_ "h5 btn block regular"] "Contact Us"
            {-div_ [class_ "col col-right pr1"] $-}
              {-a_ [href_ "#", class_ "h5 btn block regular"] "Terms of Service"-}
            {-div_ [class_ "col col-right pr1"] $-}
              {-a_ [href_ "#", class_ "h5 btn block regular"] "Privacy Policy"-}
      script_ "(function (w,i,d,g,e,t,s) {w[d] = w[d]||[];t= i.createElement(g);\
              \t.async=1;t.src=e;s=i.getElementsByTagName(g)[0];s.parentNode.insertBefore(t, s);\
              \})(window, document, '_gscq','script','//widgets.getsitecontrol.com/59273/script.js');"
  where getTitle = toHtml $ T.append "BStreamer" (maybe "" (" - " `T.append`) title)

landingView :: Html ()
landingView = topView Nothing Nothing False $ do
  div_ [class_ "p4 white bg-bc1"] $
    div_ [class_ "mx4 p4"] $
      div_ [class_ "mx4 px4"] $ do
        h1_ [class_ "h1 caps mt4 mb0 white regular"] "BStreamer"
        p_ [class_ "h3"] "In-depth broadcasting analytics"
  div_ [class_ "p3"] $
    div_ [class_ "clearfix mxn2"] $
      div_ [class_ "col-right col-8 px2"] $ do
        h2_ [class_ "caps fw400"] "BStreamer is currently in closed beta"
        div_ [class_ "py1"] $ do
          h4_ "About us"
          p_ "BStreamer is an analytics tool that uses livestream statistics and chat data \
            \ in order to provide a broadcaster with informative reports for every recorded \
            \ live session. Our goal is to create software that will help our users to get a \
            \ better understanding of their viewer interactions and assist them with growing \
            \ their channels."
          p_ "The project is currently under active development and in closed beta phase. If you \
            \ would like to get a beta access, please contact us using information below."
          h4_ "Contact Us"
          p_ $ do
            "If you would like to request beta access, leave feedback/suggestions or \
            \ report problems, please contact us using "
            a_ [href_ "#", data_ "gsc-widget" "116114", class_ "text-decoration-none"] "this form"
            " or by sending an email to support@bstreamer.io."

messageView :: T.Text -> Html ()
messageView msg = topView Nothing Nothing False $
  div_ [class_ "col-6 mx-auto mt4"] $
    p_ [class_ "center"] (toHtml msg)
