{-# LANGUAGE TypeFamilies #-}
module BStreamer.Web.Types
( UserSession
, BState(..)
, BSActionCtx
, BSpockM
, IsAdmin(..)
, initState
, termState
, runDSQuery
, getSessionUser
, checkUser
, getAdmin
) where

import Control.Monad.IO.Class (liftIO)
import Control.Monad.Trans.Class (lift)
import Control.Monad.Trans.Except (ExceptT(..), runExceptT)
import Data.HVect (HVect(..), ListContains, findFirst)
import Web.Spock

import BStreamer.Config
import BStreamer.DataStorage
import BStreamer.Types
import BStreamer.Types.User

type UserSession = Maybe User

data BState = BState {
    bConf :: BSettings
  , bDsConn :: DSConn
}

type BSActionCtx ctx = SpockActionCtx ctx () UserSession BState

type BSpockM = SpockM () UserSession BState

data IsAdmin = IsAdmin

initState :: IO (Either BSError BState)
initState = runExceptT $ do
  settings <- lift readSettings
  dsConn <- ExceptT $ connectDS (bsDSConf settings)
  return $ BState settings dsConn

termState :: BState -> IO ()
termState (BState _ dsConn) = disconnectDS dsConn

runDSQuery :: (DSConn -> IO a) -> BSActionCtx ctx a
runDSQuery f = do
  state <- getState
  liftIO $ f (bDsConn state)

getSessionUser :: ListContains n User xs => BSActionCtx (HVect xs) User
getSessionUser = fmap findFirst getContext

getAdmin :: ListContains n IsAdmin xs => BSActionCtx (HVect xs) IsAdmin
getAdmin = fmap findFirst getContext

checkUser :: ListContains n User xs => Channel -> BSActionCtx (HVect xs) Bool
checkUser chan = do
  user <- getSessionUser
  return (uChan user == chan || uRole user == RAdmin)
