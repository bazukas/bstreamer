{-# LANGUAGE DataKinds #-}
module BStreamer.Web.Urls where

import Data.Text (Text)
import Web.Routing.Combinators (PathState(..))
import Web.Spock (Path, (<//>), var)

loginPath :: Path '[] 'Open
loginPath = "auth/login"

logoutPath :: Path '[] 'Open
logoutPath = "auth/logout"

oauthPath :: Path '[] 'Open
oauthPath = "auth/oauth"

dashboardPath :: Path '[] 'Open
dashboardPath = "dashboard"

dashboardPagePath :: Path '[Int] 'Open
dashboardPagePath = dashboardPath <//> "p" <//> var

broadcastPath :: Path '[Text] 'Open
broadcastPath = dashboardPath <//> "broadcasts" <//> var

broadcastDataPath :: Path '[Text] 'Open
broadcastDataPath = broadcastPath <//> "data"

broadcastVodsPath :: Path '[Text] 'Open
broadcastVodsPath = broadcastPath <//> "vods"

chatLogsPath :: Path '[Text, Int, Int] 'Open
chatLogsPath = "chat" <//> var <//> var <//> var

adminBroadcastsPath :: Path '[] 'Open
adminBroadcastsPath = "admin/broadcasts"

adminBroadcastsPagePath :: Path '[Int] 'Open
adminBroadcastsPagePath = adminBroadcastsPath <//> "p" <//> var
