{-# OPTIONS_GHC -fno-warn-orphans #-}
module BStreamer.Web.SessionStore
( newMongoSessionStore
) where

import Data.Bson as B (Field(..), Val(..), Value(..), val, lookup, (=:))
import Data.Maybe (catMaybes)
import Data.Text (Text)
import Data.Typeable (Typeable)
import Web.Spock.Config
import Web.Spock.Internal.SessionManager (Session(..), SessionId)

import BStreamer.DataStorage

instance Eq (Session conn sess st) where
  s1 == s2 = sess_id s1 == sess_id s2

instance (Val sess, Typeable conn, Typeable st) => Val (Session conn sess st) where
  val (Session sid csrf vu sdata) = Doc
    [ "id" := val sid
    , "csrf" := val csrf
    , "valid_until" := val vu
    , "data" := val sdata
    ]
  cast' (Doc doc) = do
    sid <- B.lookup "id" doc
    csrf <- B.lookup "csrf" doc
    vu <- B.lookup "valid_until" doc
    sdata <- B.lookup "data" doc
    return $ Session sid csrf vu sdata
  cast' _ = Nothing

sessionCollection :: Text
sessionCollection = "session"

loadSession :: (Typeable conn, Typeable st, Val sess) => DSConn -> SessionId -> IO (Maybe (Session conn sess st))
loadSession conn sid = get <$> getDocuments sessionCollection ["id" =: sid] conn
  where get [] = Nothing
        get (u:_) = u

deleteSession :: DSConn -> SessionId -> IO ()
deleteSession conn sid = deleteDocuments sessionCollection ["id" =: sid] conn

storeSession :: (Typeable conn, Typeable st, Val sess) => DSConn -> Session conn sess st -> IO ()
storeSession conn sess = upsertDocument sessionCollection ["id" =: sess_id sess] sess conn

getSessions :: (Typeable conn, Typeable st, Val sess) => DSConn -> IO [Session conn sess st]
getSessions conn = catMaybes <$> getDocuments sessionCollection [] conn

filterSessions :: (Typeable conn, Typeable st, Val sess) => DSConn -> (Session conn sess st -> Bool) -> IO ()
filterSessions conn pr = do
  sessions <- getSessions conn
  let deleteKeys = map sess_id $ filter (not . pr) sessions
  mapM_ (deleteSession conn) deleteKeys

mapSessions :: (Typeable conn, Typeable st, Val sess) => DSConn -> (Session conn sess st -> IO (Session conn sess st)) -> IO ()
mapSessions conn f = do
  sessions <- getSessions conn
  mapM_ updateSession sessions
  where updateSession s = do
          ns <- f s
          upsertDocument sessionCollection ["id" =: sess_id s] ns conn

newMongoSessionStore' :: (Typeable conn, Typeable st, Val sess) => DSConn -> SessionStore (Session conn sess st) IO
newMongoSessionStore' conn = SessionStore
  { ss_runTx = id
  , ss_loadSession = loadSession conn
  , ss_deleteSession = deleteSession conn
  , ss_storeSession = storeSession conn
  , ss_toList = getSessions conn
  , ss_filterSessions = filterSessions conn
  , ss_mapSessions = mapSessions conn
  }

newMongoSessionStore :: (Typeable conn, Typeable st, Val sess) => DSConn -> SessionStoreInstance (Session conn sess st)
newMongoSessionStore = SessionStoreInstance . newMongoSessionStore'
