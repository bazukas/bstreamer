{-# LANGUAGE DataKinds #-}
module BStreamer.Web.Router
( webRoutes
) where

import Control.Monad.IO.Class (MonadIO)
import Data.HVect (HVect(..))
import Web.Spock

import BStreamer.Web.Control.Auth
import BStreamer.Web.Control.Dashboard
import BStreamer.Web.Types
import BStreamer.Web.Urls
import BStreamer.Web.Views

initHook :: MonadIO m => ActionCtxT () m (HVect '[])
initHook = return HNil

webRoutes :: BSpockM ()
webRoutes =
  prehook initHook $ do
    get root $ renderHtml landingView
    get loginPath loginUser
    get logoutPath logoutUser
    get oauthPath oAuthLogin
    prehook authHook $ do
      get dashboardPath (userBroadcasts 1)
      get dashboardPagePath userBroadcasts
      get broadcastPath broadcastReport
      get broadcastDataPath broadcastData
      get broadcastVodsPath broadcastVods
      get chatLogsPath chatLogs
      prehook adminHook $ do
        get adminBroadcastsPath (adminBroadcasts 1)
        get adminBroadcastsPagePath adminBroadcasts
