module BStreamer.Web.Views.Auth
( loginView
) where

import Data.Text (Text)
import Lucid

import BStreamer.Web.Views

twitchConnectImg :: Text
twitchConnectImg = "http://ttv-api.s3.amazonaws.com/assets/connect_dark.png"

loginView :: Text -> Html ()
loginView link = topView Nothing (Just "Login") False $
  div_ [class_ "col-2 mx-auto mt4"] $
    a_ [href_ link] $
      img_ [src_ twitchConnectImg, class_ "block mx-auto"]
