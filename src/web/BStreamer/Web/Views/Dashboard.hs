{-# LANGUAGE DataKinds #-}
module BStreamer.Web.Views.Dashboard
( broadcastsView
, broadcastView
) where

import Control.Monad (when)
import Data.Maybe (fromMaybe, fromJust, isJust)
import Data.Time.Clock (diffUTCTime)
import Data.Time.Format (formatTime, defaultTimeLocale)
import Lucid
import Text.Printf (printf)
import Web.Spock (renderRoute)
import qualified Data.Text as T

import BStreamer.Config
import BStreamer.Time
import BStreamer.Types.Broadcast
import BStreamer.Types.Broadcast.Data
import BStreamer.Util
import BStreamer.Web.Views
import BStreamer.Web.Urls

statusView :: AnalyzeStatus -> Html ()
statusView Scheduled = span_ [class_ "bold inline-block px1 white bg-bc3 rounded"] "Scheduled"
statusView InProgress = span_ [class_ "bold inline-block px1 black bg-yellow rounded"] "In Progress"
statusView Success = span_ [class_ "bold inline-block px1 white bg-green rounded"] "Success"
statusView Error = span_ [class_ "bold inline-block px1 white bg-red rounded"] "Error"

pagerButtonsNum :: Int
pagerButtonsNum = 3

renderPageRoute :: Bool -> Int -> T.Text
renderPageRoute True page = renderRoute adminBroadcastsPagePath page
renderPageRoute False page = renderRoute dashboardPagePath page

pageButton :: Bool -> Bool -> Int -> Html ()
pageButton isadmin primary page = a_ [phref primary, pclass primary] $ toHtml (show page)
  where pclass False = class_ "btn regular "
        pclass True = class_ "btn btn-primary bg-gray regular"
        phref False = href_ (renderPageRoute isadmin page)
        phref True = href_ ""

pagerView :: Bool -> Int -> Int -> Html ()
pagerView isadmin p mp = div_ [class_ "clearfix"] $ do
  a_ prevAttr "← Previous"
  a_ nextAttr "Next →"
  div_ [class_ "overflow-hidden center"] $ do
    foldMap (pageButton isadmin False) (lastElements pagerButtonsNum [1..(p-1)])
    pageButton isadmin True p
    foldMap (pageButton isadmin False) (take pagerButtonsNum [(p+1)..mp])
  where prevHref = renderPageRoute isadmin (p-1)
        nextHref = renderPageRoute isadmin (p+1)
        prevAttr = class_ "left btn regular" : if p == 1 then [] else [href_ prevHref]
        nextAttr = class_ "right btn regular" : if p == mp then [] else [href_ nextHref]

broadcastCardView :: Bool -> Broadcast -> Html ()
broadcastCardView isadmin br = div_ [class_ "border border-bc1 rounded mb3"] $ do
  a_ titleAttr $
    div_ [class_ "clearfix p1 bg-bc1 white"] $ do
      h2_ [class_ "col col-10 h2 my0 regular"] bctitle
      div_ [class_ "col col-right"] $ statusView (bcStatus br)
  div_ [class_ "p1"] $ do
    div_ [class_ "clearfix h5"] $ do
      table_ [class_ "col col-6"] $ do
        when (isJust (bcData br)) $ tr_ $ do
          td_ "Average Viewers:"
          td_ $ toHtml . show . bdAvgViewers . fromJust . bcData $ br
        when (isJust (bcData br)) $ tr_ $ do
          td_ "Average Chat Activity:"
          td_ $ toHtml . show . bdAvgChatActivity . fromJust . bcData $ br
        when (isJust (bcData br)) $ tr_ $ do
          td_ "Followers Gained:"
          td_ $ toHtml . show . bdFollowersGained . fromJust . bcData $ br
      table_ [class_ "col col-6"] $ do
        tr_ $ do
          td_ "Start Time:"
          td_ [class_ "btime"] startTime
        tr_ $ do
          td_ "End Time:"
          td_ [class_ "btime"] endTime
        tr_ $ do
          td_ "Duration:"
          td_ duration
    when bcSuccess $
      a_ [class_ "btn p0 mt1 regular blue h5", href_ titleHref] "Open Report"
  where startTime = toHtml $ formatTime defaultTimeLocale "%Y-%m-%d %H:%M" (bcDateStart br)
        endTime = toHtml $ formatTime defaultTimeLocale "%Y-%m-%d %H:%M" (bcDateEnd br)
        duration = toHtml $ formatDiffTime (diffUTCTime (bcDateEnd br) (bcDateStart br))
        titleAttr = [href_ titleHref | bcSuccess]
        titleHref = renderRoute broadcastPath (bcId br)
        bcSuccess = bcStatus br == Success
        title = fromMaybe "Broadcast" (bdTitle <$> bcData br)
        bctitle = toHtml $ T.append (if isadmin then T.append (bcChannel br) ": " else "") title

broadcastsView :: Bool -> Int -> Int -> [Broadcast] -> Html ()
broadcastsView isadmin p mp broadcasts = topView (Just header) (Just "Dashboard") True $ do
  div_ [class_ "col-4 mx-auto my2"] $ do
    h1_ [class_ "h1 center mb3"] "Recent broadcasts"
    if not (null broadcasts) then do
      foldMap (broadcastCardView isadmin) broadcasts
      pagerView isadmin p mp
    else div_ [class_ "h4 center"] "There are no recorded broadcasts yet"
  script_ "bs.formatLocal('.btime');"
  where header = script' "/static/js/bstreamer.min.js"

script' :: T.Text -> Html ()
script' href = script_ [src_ href] ("" :: String)

broadcastView :: Broadcast -> TwitchConf -> Html ()
broadcastView br tw = topView (Just header) (Just "Broadcast Report") True $ do
  div_ [class_ "col-8 mx-auto my2"] $ do
    h1_ [class_ "h1 center mb2"] $ toHtml $ fromMaybe "Broadcast" (bdTitle <$> bcData br)
    div_ [class_ "clearfix mxn2"] $ do
      div_ [class_ "col col-8 px2"] $ div_ [id_ "reportGraph", class_ "relative"] ""
      div_ [class_ "col col-4 px2"] $ do
        div_ [id_ "reportPlayer", class_ "relative"] ""
        div_ [id_ "reportChat", class_ "relative"] ""
  script_ $ T.pack $ printf "bs.initReport('reportGraph', 'reportPlayer', 'reportChat', '%s', '%s', '%s');" (bcId br) (bcChannel br) (twClientId tw)
  where
    header = do
      link_ [rel_ "stylesheet", href_ "/static/css/report.css"]
      script' "https://player.twitch.tv/js/embed/v1.js"
      script' "/static/js/bstreamer.min.js"
