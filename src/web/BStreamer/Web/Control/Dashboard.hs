{-# LANGUAGE TypeFamilies #-}
module BStreamer.Web.Control.Dashboard
( userBroadcasts
, adminBroadcasts
, broadcastReport
, broadcastData
, broadcastVods
, chatLogs
) where

import Control.Monad (when)
import Control.Monad.IO.Class (liftIO)
import Data.HVect (HVect(..), ListContains)
import Data.Text (Text)
import Data.Time.Clock.POSIX (posixSecondsToUTCTime)
import Network.HTTP.Types.Status (notFound404, forbidden403)
import Web.Spock

import BStreamer.Config
import BStreamer.Twitch
import BStreamer.Types.Broadcast
import BStreamer.Types.ChatMessage
import BStreamer.Types.User
import BStreamer.Web.Types
import BStreamer.Web.Views
import BStreamer.Web.Views.Dashboard
import BStreamer.Web.Urls

pageToLimitOffset :: Int -> Int -> (Int, Int)
pageToLimitOffset p pp = (pp, (p - 1) * pp)

maxPage :: Int -> Int -> Int
maxPage len pp = ceiling $ fromIntegral len / (fromIntegral pp :: Double)

userBroadcasts :: ListContains n User xs => Int -> BSActionCtx (HVect xs) ()
userBroadcasts page = do
  chan <- fmap uChan getSessionUser
  bpp <- fmap (wBroadcastsPerPage . bsWebConf . bConf) getState
  bcount <- runDSQuery $ getChannelBroadcastCount chan
  mBroadcasts <- runDSQuery $ uncurry (getChannelBroadcasts chan) (pageToLimitOffset page bpp)
  case mBroadcasts of
    Nothing -> renderHtml $ messageView "Error while retrieving channel broadcasts"
    Just broadcasts ->
      renderHtml $ broadcastsView False page (maxPage bcount bpp) broadcasts

adminBroadcasts :: ListContains n IsAdmin xs => Int -> BSActionCtx (HVect xs) ()
adminBroadcasts page = do
  _ <- getAdmin
  bpp <- fmap (wBroadcastsPerPage . bsWebConf . bConf) getState
  bcount <- runDSQuery getBroadcastCount
  mBroadcasts <- runDSQuery $ uncurry getBroadcasts (pageToLimitOffset page bpp)
  case mBroadcasts of
    Nothing -> renderHtml $ messageView "Error while retrieving broadcasts"
    Just broadcasts ->
      renderHtml $ broadcastsView True page (maxPage bcount bpp) broadcasts

broadcastReport :: ListContains n User xs => Text -> BSActionCtx (HVect xs) ()
broadcastReport bId = do
  twConf <- fmap (bsTwitchConf . bConf) getState
  mBroadcast <- runDSQuery $ getBroadcast bId
  case mBroadcast of
    Nothing -> redirect (renderRoute dashboardPath)
    Just broadcast -> do
      when (bcStatus broadcast /= Success) $
        redirect $ renderRoute dashboardPath
      allow <- checkUser $ bcChannel broadcast
      if allow
        then renderHtml $ broadcastView broadcast twConf
        else redirect $ renderRoute dashboardPath

broadcastData :: ListContains n User xs => Text -> BSActionCtx (HVect xs) ()
broadcastData bId = do
  mBroadcast <- runDSQuery $ getBroadcast bId
  case mBroadcast of
    Nothing -> setStatus notFound404
    Just broadcast -> do
      allow <- checkUser $ bcChannel broadcast
      if allow
        then case bcData broadcast of
          Nothing -> setStatus notFound404
          Just bData -> json bData
        else setStatus forbidden403

broadcastVods :: ListContains n User xs => Text -> BSActionCtx (HVect xs) ()
broadcastVods bId = do
  twConf <- fmap (bsTwitchConf . bConf) getState
  mBroadcast <- runDSQuery $ getBroadcast bId
  case mBroadcast of
    Nothing -> setStatus notFound404
    Just broadcast -> do
      allow <- checkUser $ bcChannel broadcast
      if allow
        then do
          vods <- liftIO $ getChannelVods twConf (bcChannel broadcast) (bcDateStart broadcast) (bcDateEnd broadcast)
          json vods
        else setStatus forbidden403

chatLogs :: ListContains n User xs => Text -> Int -> Int -> BSActionCtx (HVect xs) ()
chatLogs chan ds de = do
  allow <- checkUser chan
  if allow
    then do
      msgs <- runDSQuery $ getChatMessages chan (intToUTC ds) (intToUTC de)
      json msgs
    else setStatus forbidden403
  where intToUTC = posixSecondsToUTCTime . fromIntegral
