{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeOperators #-}
module BStreamer.Web.Control.Auth
( authHook
, adminHook
, getSessionUser
, oAuthLogin
, loginUser
, logoutUser
) where

import Control.Monad.IO.Class (liftIO)
import Control.Monad.Trans.Maybe (MaybeT(..))
import Data.HVect (HVect(..), ListContains)
import Web.Spock

import BStreamer.Config
import BStreamer.Twitch
import BStreamer.Types.User
import BStreamer.Web.Types
import BStreamer.Web.Views
import BStreamer.Web.Views.Auth
import BStreamer.Web.Urls

authHook :: BSActionCtx (HVect xs) (HVect (User ': xs))
authHook = do
  oldCtx <- getContext
  mUser <- readSession
  case mUser of
    Nothing -> redirect $ renderRoute loginPath
    Just user -> return (user :&: oldCtx)

adminHook :: ListContains n User xs => BSActionCtx (HVect xs) (HVect (IsAdmin ': xs))
adminHook = do
  oldCtx <- getContext
  user <- getSessionUser
  if isAdmin user
    then return (IsAdmin :&: oldCtx)
    else redirect $ renderRoute dashboardPath

loginUser :: BSActionCtx ctx ()
loginUser = do
  mUser <- readSession
  case mUser of
    Nothing -> do
      state <- getState
      renderHtml $ loginView $ getTwitchOAuth (bsTwitchConf . bConf $ state)
    Just user -> do
      sessionRegenerateId
      redirect $ renderRoute (if isAdmin user then adminBroadcastsPath else dashboardPath)

oAuthLogin :: BSActionCtx ctx ()
oAuthLogin = do
  state <- getState
  mUser <- runMaybeT $ do
    code <- MaybeT $ param "code"
    token <- MaybeT $ liftIO $ getAccessToken (bsTwitchConf . bConf $ state) code
    chan <- MaybeT $ liftIO $ getChannelName token
    MaybeT $ runDSQuery (getUser chan)
  case mUser of
    Nothing -> renderHtml . messageView $ "Unable to authenticate"
    Just user -> do
      sessionRegenerateId
      writeSession $ Just user
      redirect $ renderRoute (if isAdmin user then adminBroadcastsPath else dashboardPath)

logoutUser :: BSActionCtx ctx ()
logoutUser = do
  writeSession Nothing
  renderHtml . messageView $ "Logout successful"
