module BStreamer.Web
( app
, appConf
) where

import Control.Monad.IO.Class (liftIO)
import Web.Spock.Config

import BStreamer.Log
import BStreamer.Web.SessionStore
import BStreamer.Web.Router
import BStreamer.Web.Types

logger :: String
logger = "bstreamer.web"

appConf :: BState -> IO (SpockCfg () UserSession BState)
appConf st = do
  defSess <- defaultSessionCfg Nothing
  cfg <- defaultSpockCfg Nothing PCNoDatabase st
  return $ cfg { spc_sessionCfg = defSess
                                  { sc_store = newMongoSessionStore (bDsConn st)
                                  , sc_sessionTTL = 60 * 60 * 24
                                  }
               }

app :: BSpockM ()
app = do
  liftIO $ logInfo logger "Starting web server"
  webRoutes
