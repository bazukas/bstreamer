module BStreamer.Viewer
( mainLoop
, chatCallback
, chatEventCallback
) where

import Control.Monad
import Control.Monad.Trans.Class (lift)
import Control.Monad.Trans.Reader
import Control.Concurrent (threadDelay)
import Control.Concurrent.Async (mapConcurrently)
import Control.Concurrent.MVar (readMVar)
import Data.Maybe
import Data.Time.Clock (diffUTCTime, getCurrentTime)
import qualified Data.Map.Strict as Map
import qualified Data.Set as Set

import BStreamer.AMQP
import BStreamer.Config
import BStreamer.DataStorage
import BStreamer.Log
import BStreamer.Types
import BStreamer.Types.Broadcast
import BStreamer.Types.ChannelStat
import BStreamer.Types.ChatEvent
import BStreamer.Types.ChatMessage
import BStreamer.Types.User
import BStreamer.Twitch
import BStreamer.Viewer.Env
import BStreamer.Viewer.Irc

logger :: String
logger = "bstreamer.viewer"

processStreamData :: Channel -> BSEnvT IO ()
processStreamData chan = do
  lift $ logDebugL logger ["Schedule processing of ", chan, "'s stream data"]
  dsConn <- asks envDSConn
  amqpConn <- asks envAMQPConn
  statsM <- lift $ deleteChannelStats chan dsConn
  case statsM of
    Nothing -> return ()
    Just stats -> unless (null stats) $ do
      let minTs = minimum $ fmap csTimestamp stats
      let maxTs = maximum $ fmap csTimestamp stats
      mId <- lift $ newBroadcast chan minTs maxTs stats dsConn
      case mId of
        Nothing -> lift $ logWarningL logger ["Could not save broadcast for channel ", chan]
        Just oid -> lift $ publishAnalyzeTask amqpConn oid

chatCallback :: DSConn -> ChatCallback
chatCallback conn cm = insertCM cm conn

chatEventCallback :: DSConn -> EventCallback
chatEventCallback conn ce = insertCE ce conn

channelThread :: Channel -> Int -> BSEnvT IO ()
channelThread chan c = do
  asks (vStatPeriod . bsViewerConf . envConf) >>= lift . threadDelay . (*1000000)
  twConf <- asks (bsTwitchConf . envConf)
  statM <- lift $ getChannelStat twConf chan
  case statM of
    Nothing -> do
      retryCount <- asks (vStatRetries . bsViewerConf . envConf)
      if c == (retryCount-1) then chanIsOffline chan
      else channelThread chan (c+1)
    Just stat -> do
      asks envDSConn >>= lift . insertStat stat
      channelThread chan 0

chanIsOnline :: Channel -> BSEnvT IO ()
chanIsOnline chan = do
  lift $ logDebugL logger [chan, " is online"]
  initThread chan $ do
    asks envIrcConn >>= lift . joinChan chan
    channelThread chan 0

chanIsOffline :: Channel -> BSEnvT IO ()
chanIsOffline chan = do
  lift $ logDebugL logger [chan, " went offline"]
  termThread chan $ do
    asks envIrcConn >>= lift . leaveChan chan
    processStreamData chan

checkChannel :: Channel -> BSEnvT IO ()
checkChannel chan = do
  twConf <- asks (bsTwitchConf . envConf)
  statM <- lift $ getChannelStat twConf chan
  case statM of
    Nothing -> return ()
    Just stat -> do
      asks envDSConn >>= lift . insertStat stat
      chanIsOnline chan

restoreLiveThreads :: BSEnvT IO ()
restoreLiveThreads = do
  lift $ logInfo logger "Restoring live threads"
  curChannels <- lift . getCurChannels =<< asks envDSConn
  now <- lift getCurrentTime
  mapM_ (check now) (fromMaybe [] curChannels)
  where check now chan = do
          tsM <- lift . getLastTimestamp chan =<< asks envDSConn
          when (isJust tsM) $ do
            let ts = fromJust tsM
            restorePeriod <- asks (vRestorePeriod . bsViewerConf . envConf)
            if now `diffUTCTime` ts < fromIntegral restorePeriod then chanIsOnline chan
            else processStreamData chan

mainLoop :: BSEnvT IO ()
mainLoop = do
  restoreLiveThreads
  lift $ logInfo logger "Starting main loop"
  dsConn <- asks envDSConn
  forever $ do
    liveChans <- asks envLiveThreads >>= lift . readMVar
    allChannels <- lift $ maybe [] (fmap uChan) <$> getAllUsers dsConn
    let chansToCheck = Set.toList $ Set.difference (Set.fromList allChannels) (Map.keysSet liveChans)
    ask >>= \st -> lift . void $ mapConcurrently (readerCheckChan st) chansToCheck
    asks (vCheckPeriod . bsViewerConf . envConf) >>= lift . threadDelay . (*1000000)
  where
    readerCheckChan state chan = runReaderT (checkChannel chan) state
