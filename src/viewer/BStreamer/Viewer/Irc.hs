module BStreamer.Viewer.Irc
( ChatCallback
, EventCallback
, IrcSettings(..)
, IrcConn
, connectIrcServer
, disconnectIrcServer
, joinChan
, leaveChan
, checkListenLoop
, checkLastTimestamp
) where

import Control.Concurrent (threadDelay)
import Control.Concurrent.Async (Async, async, poll, cancel, waitCatch)
import Control.Concurrent.MVar (MVar, newEmptyMVar, withMVar, putMVar, takeMVar, newMVar, readMVar, swapMVar, modifyMVar_, tryTakeMVar)
import Control.Lens (view)
import Control.Monad (void, when, forever)
import Data.Either (isLeft)
import Data.List (find)
import Data.Maybe (fromMaybe, catMaybes)
import Data.Text.Read (decimal)
import Data.Time.Clock (UTCTime(..), getCurrentTime, diffUTCTime)
import Irc.Commands (ircNick, ircCapReq, ircJoin, ircPart, ircPong)
import Irc.Identifier (mkId, idText)
import Irc.RateLimit (RateLimit, newRateLimit, tickRateLimit)
import Irc.RawIrcMsg (RawIrcMsg, TagEntry(..), renderRawIrcMsg, parseRawIrcMsg,
  asUtf8, msgCommand, msgParams, msgPrefix, msgTags)
import Irc.UserInfo (UserInfo(..))
import Network (PortID(..), connectTo)
import System.IO (Handle, BufferMode(..), IOMode(..), hSetBuffering, hClose, openFile, hWaitForInput)
import System.IO.Error (tryIOError)
import Text.Read (readMaybe)
import qualified Data.ByteString as BS
import qualified Data.Set as Set
import qualified Data.Text as T

import BStreamer.Config
import BStreamer.Log
import BStreamer.Types
import BStreamer.Types.ChatEvent
import BStreamer.Types.ChatMessage
import BStreamer.Types.StreamEvent

type ChatCallback = ChatMessage -> IO ()
type EventCallback = ChatEvent -> IO ()

data IrcSettings = IrcSettings {
    mIrcConf :: IrcConf
  , mIrcChatCb :: ChatCallback
  , mIrcEventCb :: EventCallback
}

data IrcConn' = IrcConn' {
    _mIrcConn :: MVar Handle
  , _mIrcChannels :: MVar (Set.Set Channel)
  , _mLastMsg :: MVar UTCTime
  , _mDebugFile :: Maybe Handle
  , _mRateLimit :: RateLimit
  , _mSettings :: IrcSettings
}

type IrcConn = (IrcConn', MVar (Async ()))

logger :: String
logger = "bstreamer.viewer.irc"

connectIrcServer :: IrcSettings -> IO IrcConn
connectIrcServer settings = do
  logInfo logger "Connecting to irc server"
  let conf = mIrcConf settings
  debug <- if ircDebug conf
            then openDebug
            else return Nothing
  rl <- newRateLimit (ircRateLimitPenalty conf) (ircRateLimitThreshold conf)
  mirc <- newEmptyMVar
  mchans <- newMVar Set.empty
  mtime <- getCurrentTime >>= newMVar
  let conn = IrcConn' mirc mchans mtime debug rl settings
  connectIrcServer' conn
  as <- async $ listenLoop conn
  mas <- newMVar as
  return (conn, mas)
  where
    openDebug = do
      h <- openFile "irc_debug.log" AppendMode
      hSetBuffering h NoBuffering
      return $ Just h

connectIrcServer' :: IrcConn' -> IO ()
connectIrcServer' conn@(IrcConn' mirc mchans mtime _ _ settings) = do
  let conf = mIrcConf settings
  irc' <- tryTakeMVar mirc
  mapM_ (tryIOError . hClose) irc'
  irc <- connectTo (ircHost conf) (PortNumber (fromIntegral (ircPort conf)))
  hSetBuffering irc NoBuffering
  putMVar mirc irc
  void $ getCurrentTime >>= swapMVar mtime
  sendCommand conn $ ircNick (ircUser conf)
  wsucc <- waitWelcome conn
  if wsucc then do
             sendCommand conn $ ircCapReq ["twitch.tv/tags"]
             sendCommand conn $ ircCapReq ["twitch.tv/commands"]
             withMVar mchans $ mapM_ (\ch -> sendCommand conn $ ircJoin (T.append "#" ch) Nothing)
           else do
             logWarning logger "Could not get welcome message from server"
             reconnect conn

reconnect :: IrcConn' -> IO ()
reconnect conn@(IrcConn' _ _ _ _ _ settings) = do
  let rp = ircReconnectPeriod . mIrcConf $ settings
  logInfoL logger ["Reconnecting to irc server in ", T.pack . show $ rp, " seconds"]
  threadDelay (rp * 1000000)
  connectIrcServer' conn

disconnectIrcServer :: IrcConn -> IO ()
disconnectIrcServer (IrcConn' mirc _ _ debug _ _, mas) = do
  logInfo logger "Disconnecting from irc server"
  as <- takeMVar mas
  cancel as
  void $ waitCatch as
  mbirc <- tryTakeMVar mirc
  mapM_ hClose mbirc
  mapM_ hClose debug

joinChan :: Channel -> IrcConn -> IO ()
joinChan chan (conn, _) = do
  logDebugL logger ["Joining ", chan, "'s chat"]
  modifyMVar_ (_mIrcChannels conn) (return . Set.insert chan)
  sendCommand conn $ ircJoin (T.append "#" chan) Nothing

leaveChan :: Channel -> IrcConn -> IO ()
leaveChan chan (conn, _) = do
  logDebugL logger ["Leaving ", chan, "'s chat"]
  modifyMVar_ (_mIrcChannels conn) (return . Set.delete chan)
  sendCommand conn $ ircPart (mkId $ T.append "#" chan) ""

sendCommand :: IrcConn' -> RawIrcMsg -> IO ()
sendCommand (IrcConn' mirc _ _ debug rl _) msg = do
  tickRateLimit rl
  let bs = renderRawIrcMsg msg
  mapM_ (`BS.hPut` BS.concat ["< ", bs]) debug
  void $ withMVar mirc (\irc -> tryIOError $ BS.hPut irc bs)

listenLoop :: IrcConn' -> IO ()
listenLoop conn@(IrcConn' mirc _ mtime debug _ s) = forever $ do
  eready <- withMVar mirc (tryIOError . (`hWaitForInput` 200))
  case eready of
    Left err -> do
      logWarningL logger ["Received error on irc socket: ", T.pack . show $ err]
      reconnect conn
    Right True -> do
      eraw <- withMVar mirc (tryIOError . BS.hGetLine)
      case eraw of
        Left err -> do
          logWarningL logger ["Received error on irc socket: ", T.pack . show $ err]
          reconnect conn
        Right raw -> do
          let mMsg = parseRawIrcMsg . asUtf8 $ raw
          case mMsg of
            Nothing -> return ()
            Just msg -> do
              mapM_ (`BS.hPut` BS.concat ["> ", renderRawIrcMsg msg]) debug
              ts <- getCurrentTime
              void $ swapMVar mtime ts
              case view msgCommand msg of
                "PING" -> sendCommand conn (ircPong (view msgParams msg))
                "RECONNECT" -> do
                  logInfo logger "Received RECONNECT command"
                  reconnect conn
                "PRIVMSG" -> handleMsg s msg ts
                "ROOMSTATE" -> handleRoomState s msg
                "CLEARCHAT" -> handleClearChat s msg
                _ -> return ()
    Right False -> threadDelay 200

waitWelcome :: IrcConn' -> IO Bool
waitWelcome (IrcConn' mirc _ _ _ _ _) = do
  eraw <- withMVar mirc (tryIOError . BS.hGetLine)
  case eraw of
    Left err -> do
      logWarningL logger ["Received error on irc socket: ", T.pack . show $ err]
      return False
    Right raw -> do
      let mMsg = parseRawIrcMsg . asUtf8 $ raw
      case mMsg of
        Nothing -> return False
        Just msg -> return (view msgCommand msg == "001")

handleMsg :: IrcSettings -> RawIrcMsg -> UTCTime -> IO ()
handleMsg (IrcSettings _ cb _) msg ts
  | Just user <- view msgPrefix msg
  , [chan,txt] <- view msgParams msg = do
    let tags = view msgTags msg
    cb $ ChatMessage (T.tail chan) ts (getUser user) (T.stripEnd txt) (getColor tags) (getBadges tags) (getEmotes tags)
  | otherwise = return ()
  where getUser = idText . userNick
        getColor tags = tagValue <$> findTag "color" tags
        getBadges tags = maybe [] parseBadges $ findTag "badges" tags
        parseBadges (TagEntry _ v) = fromMaybe [] (mapM (parseBadge . T.dropEnd 2) $ T.splitOn "," v)
        getEmotes tags = maybe [] parseEmotes $ findTag "emotes" tags
        parseEmotes (TagEntry _ v) = fromMaybe [] (mapM parseEmote $ T.splitOn "/" v)
        findTag tag = find (\(TagEntry t _) -> t == tag)
        tagValue (TagEntry _ v) = v

handleRoomState :: IrcSettings -> RawIrcMsg -> IO ()
handleRoomState (IrcSettings _ _ cb) msg
  | [chan] <- view msgParams msg = do
    let chan' = T.stripEnd . T.tail $ chan
    ts <- getCurrentTime
    let mevents = catMaybes $ parseEvent (ChatEvent chan' ts) <$> view msgTags msg
    mapM_ cb mevents
  | otherwise = return ()
  where parseNum t = readMaybe (T.unpack t) :: Maybe Int
        parseEvent rec (TagEntry "subs-only" v) =
          if v == "0"
            then Just $ rec SubscriberOnlyOff
            else Just $ rec SubscriberOnlyOn
        parseEvent rec (TagEntry "slow" v) =
          if v == "0"
            then Just $ rec SlowModeOff
            else (rec . SlowMode) <$> parseNum v
        parseEvent rec (TagEntry "r9k" v) =
          if v == "0"
            then Just $ rec R9KOff
            else Just $ rec R9KOn
        parseEvent rec (TagEntry "emote-only" v) =
          if v == "0"
            then Just $ rec EmoteOnlyOff
            else Just $ rec EmoteOnlyOn
        parseEvent _ _ = Nothing

handleClearChat :: IrcSettings -> RawIrcMsg -> IO ()
handleClearChat (IrcSettings _ _ cb) msg =
  case view msgParams msg of
    [chan] -> do
      let chan' = T.stripEnd . T.tail $ chan
      ts <- getCurrentTime
      cb $ ChatEvent chan' ts ClearChat
      return ()
    [chan,user] -> do
      let chan' = T.stripEnd . T.tail $ chan
      let user' = T.stripEnd user
      ts <- getCurrentTime
      case view msgTags msg of
        [TagEntry "ban-reason" r] -> cb $ ChatEvent chan' ts (Ban user' r)
        [TagEntry "ban-duration" d, TagEntry "ban-reason" r] ->
          case decimal d of
            Left _ -> return ()
            Right (d', _) -> cb $ ChatEvent chan' ts (TimeOut user' d' r)
        _ -> return ()
    _ -> return ()

checkListenLoop :: IrcConn -> IO ()
checkListenLoop (conn, mas) = do
  as <- readMVar mas
  res <- poll as
  case res of
    Nothing -> return ()
    Just e -> when (isLeft e) $ do
      logError logger (IrcError $ show e) "checking on listen loop"
      logWarning logger "Listen loop thread has returned with an error, restarting.."
      nas <- async $ listenLoop conn
      void $ swapMVar mas nas

checkLastTimestamp :: IrcConn -> IO ()
checkLastTimestamp (IrcConn' mirc _ mtime _ _ settings, _) = do
  let conf = mIrcConf settings
  withMVar mtime $ \ts -> do
    currentTs <- getCurrentTime
    when ((currentTs `diffUTCTime` ts) > fromIntegral (ircPingTimeout conf)) $ do
      logWarning logger "Irc ping time out, closing handle"
      withMVar mirc (void . tryIOError . hClose)
