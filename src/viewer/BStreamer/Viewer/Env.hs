module BStreamer.Viewer.Env
( BSEnvT
, BSEnv(..)
, BSHandlers(..)
, ThreadInfo(..)
, MThreadMap
, createEnv
, destroyEnv
, initThread
, termThread
) where

import Control.Concurrent.Async (Async, async, waitCatch, cancel, asyncThreadId, poll)
import Control.Concurrent (myThreadId, threadDelay, forkIO)
import Control.Concurrent.MVar (MVar, newMVar, takeMVar, putMVar, readMVar)
import Control.Monad (when, void, forever)
import Control.Monad.Trans.Class (lift)
import Control.Monad.Trans.Except (ExceptT(..))
import Control.Monad.Trans.Reader
import Data.Either (isLeft)
import qualified Data.Map.Strict as Map

import BStreamer.AMQP
import BStreamer.Config
import BStreamer.DataStorage
import BStreamer.Log
import BStreamer.Types
import BStreamer.Viewer.Irc

type ThreadMap = Map.Map Channel ThreadInfo
type MThreadMap = MVar ThreadMap

data ThreadInfo = ThreadInfo {
    tChannel :: Channel
  , tAsync :: Async ()
}

data BSEnv = BSEnv {
    envConf :: BSettings
  , envDSConn :: DSConn
  , envIrcConn :: IrcConn
  , envAMQPConn :: AMQPConn
  , envLiveThreads :: MThreadMap
}

type BSEnvT = ReaderT BSEnv

data BSHandlers = BSHandlers {
    hIrcChatCallback :: DSConn -> ChatCallback
  , hIrcEventCallback :: DSConn -> EventCallback
}

logger :: String
logger = "bstreamer.viewer.env"

createEnv :: BSHandlers -> ExceptT BSError IO BSEnv
createEnv handlers = do
  conf <- lift readSettings
  lift $ logInfo logger "Initializing environment"
  liveChans <- lift $ newMVar Map.empty
  dsConn <- ExceptT $ connectDS (bsDSConf conf)
  let chatCb = hIrcChatCallback handlers
      eventCb = hIrcEventCallback handlers
  ircConn <- lift $ connectIrcServer $ IrcSettings (bsIrcConf conf) (chatCb dsConn) (eventCb dsConn)
  amqpConn <- lift $ connectAMQPServer (bsAMQPConf conf)
  let env = BSEnv conf dsConn ircConn amqpConn liveChans
  void . lift . forkIO $ runReaderT liveThreadsChecker env -- start threads checker service
  return env

destroyEnv :: BSEnv -> IO ()
destroyEnv benv = do
  logInfo logger "Terminating environment"
  liveChans <- takeMVar (envLiveThreads benv)
  mapM_ destroyThreadInfo (Map.elems liveChans)
  putMVar (envLiveThreads benv) Map.empty
  disconnectDS $ envDSConn benv
  disconnectIrcServer $ envIrcConn benv
  disconnectAMQPServer $ envAMQPConn benv

createThreadInfo :: Channel -> IO () -> IO ThreadInfo
createThreadInfo chan m = do
  logDebugL logger ["Creating thread info for ", chan]
  as <- async m
  return $ ThreadInfo chan as

destroyThreadInfo :: ThreadInfo -> IO ()
destroyThreadInfo (ThreadInfo chan as) = do
  logDebugL logger ["Terminating thread info for ", chan]
  threadId <- myThreadId
  when (asyncThreadId as /= threadId) $ do
    cancel as
    void $ waitCatch as

initThread :: Channel -> BSEnvT IO () -> BSEnvT IO ()
initThread chan m = do
  lift $ logDebugL logger ["Initializing live thread for ", chan]
  env <- ask
  liveChans <- lift $ takeMVar (envLiveThreads env)
  if Map.notMember chan liveChans then do
    newThread <- lift $ createThreadInfo chan (runReaderT m env)
    lift $ putMVar (envLiveThreads env) (Map.insert chan newThread liveChans)
  else lift $ putMVar (envLiveThreads env) liveChans

termThread :: Channel -> BSEnvT IO () -> BSEnvT IO ()
termThread chan m = do
  lift $ logDebugL logger ["Terminating live thread for ", chan]
  env <- ask
  liveChans <- lift $ takeMVar (envLiveThreads env)
  (if Map.member chan liveChans then do
    lift $ destroyThreadInfo (liveChans Map.! chan)
    lift $ putMVar (envLiveThreads env) (Map.delete chan liveChans)
  else lift $ putMVar (envLiveThreads env) liveChans) >> m

liveThreadsChecker :: BSEnvT IO ()
liveThreadsChecker = forever $ do
  asks (vThreadsCheckPeriod . bsViewerConf . envConf) >>= lift . threadDelay . (*1000000)
  liveChans <- lift . readMVar =<< asks envLiveThreads
  lift $ logDebug logger "Checking on live threads"
  mapM_ checkThread (Map.elems liveChans)
  asks envIrcConn >>= lift . checkListenLoop
  asks envIrcConn >>= lift . checkLastTimestamp

checkThread :: ThreadInfo -> BSEnvT IO ()
checkThread (ThreadInfo chan as) = do
  res <- lift . poll $ as
  case res of
    Nothing -> return ()
    Just e -> when (isLeft e) $ do
      lift $ logWarning logger "Live thread has returned with an error"
      lift $ logErrorL logger (UncaughtError $ show e) ["checking on ", chan, "'s thread"]
      termThread chan (asks envIrcConn >>= lift . leaveChan chan)
