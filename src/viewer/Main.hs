module Main where

import Control.Exception as E
import Control.Monad.Trans.Except
import Control.Monad.Trans.Reader

import BStreamer.Log
import BStreamer.Types
import BStreamer.Viewer (mainLoop, chatCallback, chatEventCallback)
import BStreamer.Viewer.Env

logger :: String
logger = "bstreamer.viewer"

envHandlers :: BSHandlers
envHandlers = BSHandlers chatCallback chatEventCallback

main :: IO ()
main = E.bracket acquire release action
  where
    acquire = do
      initializeLoggers "viewer"
      runExceptT (createEnv envHandlers)
    release envE = do
      case envE of
        Left _ -> return ()
        Right env -> destroyEnv env
      terminateLoggers
    action (Left err) = logError logger err "initializing environment"
    action (Right env) = runReaderT mainLoop env `E.catches` [
        E.Handler handleAsync
      , E.Handler handleSome
      ]
    -- uncaught exception handlers
    handleAsync :: E.AsyncException -> IO ()
    handleAsync E.UserInterrupt = E.throwIO E.UserInterrupt
    handleAsync ex = handleUncaught ex
    handleSome :: E.SomeException -> IO ()
    handleSome = handleUncaught
    handleUncaught ex = do
      logError logger (UncaughtError . show $ ex) "running main program"
      E.throwIO ex
