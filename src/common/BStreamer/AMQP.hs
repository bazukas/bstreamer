module BStreamer.AMQP
( AMQPConn
, AMQPChan
, AnalyzeTask
, connectAMQPServer
, disconnectAMQPServer
, openAMQPChannel
, closeAMQPChannel
, publishAnalyzeTask
, consumeAnalyzeTasks
) where

import Control.Monad (void)
import Data.Binary (encode, decode)
import Data.Text (Text, unpack)
import Network.AMQP
import qualified Data.ByteString.Lazy as BL

import BStreamer.Config
import BStreamer.Log

type AMQPConn = Connection
type AMQPChan = Channel
type AMQPCallback = (Message, Envelope) -> IO ()

type AnalyzeTask = Text

logger :: String
logger = "bstreamer.amqp"

connectAMQPServer :: AMQPConf -> IO AMQPConn
connectAMQPServer conf = do
  logInfo logger "Connecting to amqp server"
  openConnection (unpack $ amqpHost conf) (amqpVirtualHost conf) (amqpLogin conf) (amqpPass conf)

disconnectAMQPServer :: AMQPConn -> IO ()
disconnectAMQPServer conn = do
  logInfo logger "Disconnecting from amqp server"
  closeConnection conn

-- open amqp channel and set prefetch count
openAMQPChannel :: AMQPConn -> IO AMQPChan
openAMQPChannel conn = do
  chan <- openChannel conn
  qos chan 0 1 False
  return chan

closeAMQPChannel :: AMQPChan -> IO ()
closeAMQPChannel = closeChannel

publishMessage :: AMQPConn -> Text -> BL.ByteString -> IO ()
publishMessage conn queue msg = do
  chan <- openChannel conn
  void $ declareQueue chan newQueue { queueName = queue }
  void $ publishMsg chan "" queue newMsg { msgBody = msg, msgDeliveryMode = Just Persistent }
  closeChannel chan

consumeMessages :: AMQPChan -> Text -> AMQPCallback -> IO ()
consumeMessages chan queue cb = do
  void $ declareQueue chan newQueue { queueName = queue }
  void $ consumeMsgs chan queue Ack cb

analyzeQueue :: Text
analyzeQueue = "analyze_queue_v2"

publishAnalyzeTask :: AMQPConn -> AnalyzeTask -> IO ()
publishAnalyzeTask conn task = do
  logDebugL logger ["Publishing analyze task with id: ", task]
  publishMessage conn analyzeQueue (encode task)

consumeAnalyzeTasks :: AMQPChan -> (AnalyzeTask -> IO ()) -> IO ()
consumeAnalyzeTasks chan cb = do
  logDebugL logger ["Registering consumer for analyze tasks"]
  consumeMessages chan analyzeQueue callback
  where callback (m, e) = do
          cb $ decode (msgBody m)
          ackEnv e
