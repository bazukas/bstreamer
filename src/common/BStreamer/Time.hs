module BStreamer.Time
( generateMinuteSeries
, roundTimeMinute
, formatDiffTime
) where

import Data.Time.Clock
import Text.Printf (printf)
import qualified Data.Text as T

roundTimeMinute ::  UTCTime -> UTCTime
roundTimeMinute ts = UTCTime (utctDay ts) (roundS $ utctDayTime ts)
  where roundS s = let ps = floor $ toRational s
                   in secondsToDiffTime $ ps - (ps `mod` 60)

generateMinuteSeries :: UTCTime -> UTCTime -> [UTCTime]
generateMinuteSeries ts1 ts2 = reverse $ generate' (roundTimeMinute ts1) ts2 []
  where generate' t1 t2 ts
          | t1 < t2 = generate' (addUTCTime 60 t1) t2 (t1:ts)
          | otherwise = ts

formatDiffTime :: NominalDiffTime -> T.Text
formatDiffTime dt = T.pack $ printf "%d %s %d %s" h ht m mt
  where h = dt' `div` 3600 :: Int
        ht = if h == 1 then "hour" else "hours" :: T.Text
        m = (dt' - 3600 * h) `div` 60
        mt = if m == 1 then "minute" else "minutes" :: T.Text
        dt' = floor dt
