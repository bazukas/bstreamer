module BStreamer.Util where

import Data.List
import Data.Ord

maybeToEither :: e -> Maybe a -> Either e a
maybeToEither = flip maybe Right . Left

isRight :: Either e a -> Bool
isRight (Left _) = False
isRight (Right _) = True

rightPad :: String -> Int -> String
rightPad xs m = xs ++ replicate (m - length xs) ' '

intAvg :: [Int] -> Int
intAvg xs = sum xs `div` length xs

average :: (Real a, Fractional b) => [a] -> b
average xs = realToFrac (sum xs) / fromIntegral (length xs)

mostFrequent :: Ord a => [a] -> a
mostFrequent = head . minimumBy (flip $ comparing length) . group . sort

lastElements :: Int -> [a] -> [a]
lastElements n xs = drop (length xs - n) xs

remDups :: (Eq b) => [a] -> (a -> b) -> [a]
remDups [] _ = []
remDups [x] _ = [x]
remDups (x:xx:xs) acc = if acc x == acc xx then remDups (x:xs) acc else x : remDups (xx:xs) acc

splitListHalf :: [a] -> ([a], [a])
splitListHalf xs = splitAt ((length xs + 1) `div` 2) xs
