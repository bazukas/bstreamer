module BStreamer.Log
( initializeLoggers
, terminateLoggers
, logDebug
, logDebugL
, logInfo
, logInfoL
, logWarning
, logWarningL
, logError
, logErrorL
) where

import System.Log.Formatter (simpleLogFormatter)
import System.Log.Handler (setFormatter)
import System.Log.Handler.Simple (fileHandler, streamHandler)
import System.Log.Logger
import System.IO (stdout)
import qualified Data.Text as T

import BStreamer.Types
import BStreamer.Util

bstreamerLogger :: String
bstreamerLogger = "bstreamer"

getLogPath :: T.Text -> T.Text -> FilePath
getLogPath app file = concat ["/var/log/bstreamer/", T.unpack app, "/", T.unpack file]

initializeLoggers :: T.Text -> IO ()
initializeLoggers app = do
  debugHandler <- fileHandler' "debug.log" DEBUG
  infoHandler <- fileHandler' "info.log" INFO
  stdoutHandler <- formatHandler <$> streamHandler stdout INFO
  updateGlobalLogger rootLoggerName removeHandler
  updateGlobalLogger bstreamerLogger $ setLevel DEBUG
  updateGlobalLogger bstreamerLogger $ addHandler debugHandler
  updateGlobalLogger bstreamerLogger $ addHandler infoHandler
  updateGlobalLogger bstreamerLogger $ addHandler stdoutHandler
  where fileHandler' file prio = formatHandler <$> fileHandler (getLogPath app file) prio
        formatHandler h = setFormatter h (simpleLogFormatter "[$time - $loggername - $prio] $msg")

terminateLoggers :: IO ()
terminateLoggers = removeAllHandlers

padLog :: String -> String
padLog l = rightPad l 21

logDebugL :: String -> [T.Text] -> IO ()
logDebugL logger msgs = debugM (padLog logger) (T.unpack $ T.concat msgs)

logDebug :: String -> T.Text -> IO ()
logDebug logger msg = logDebugL logger [msg]

logInfoL :: String -> [T.Text] -> IO ()
logInfoL logger msgs = infoM (padLog logger) (T.unpack $ T.concat msgs)

logInfo :: String -> T.Text -> IO ()
logInfo logger msg = logInfoL logger [msg]

logWarningL :: String -> [T.Text] -> IO ()
logWarningL logger msgs = warningM (padLog logger) (T.unpack $ T.concat msgs)

logWarning :: String -> T.Text -> IO ()
logWarning logger msg = logWarningL logger [msg]

logErrorL :: String -> BSError -> [T.Text] -> IO ()
logErrorL logger err msgs = errorM (padLog logger) $ concat [show err, " - while ", T.unpack $ T.concat msgs]

logError :: String -> BSError -> T.Text -> IO ()
logError logger err msg = logErrorL logger err [msg]
