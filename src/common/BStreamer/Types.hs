module BStreamer.Types
( BSError(..)
, Channel
) where

import Data.Text (Text)

import Control.Exception (Exception)
import Data.Typeable (Typeable)

type Channel = Text

data BSError = DbError String
             | IrcError String
             | HttpError String
             | UncaughtError String
             deriving (Show, Typeable)

instance Exception BSError
