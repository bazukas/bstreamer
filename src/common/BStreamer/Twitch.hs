{-# LANGUAGE DeriveGeneric #-}
module BStreamer.Twitch
( getChannelStat
, getAccessToken
, getTwitchOAuth
, getChannelName
, getChannelVods
) where

import Data.Aeson (FromJSON(..), decode, genericParseJSON, withObject, (.:))
import Data.Aeson.Casing (aesonPrefix, snakeCase)
import Data.Text.Encoding (encodeUtf8, decodeUtf8)
import Data.Time.Clock (UTCTime, getCurrentTime)
import GHC.Generics (Generic)
import Network.HTTP.Types (renderSimpleQuery)
import qualified Data.ByteString.Lazy as BL
import qualified Data.Text as T

import BStreamer.Config
import BStreamer.Http
import BStreamer.Log
import BStreamer.Types
import BStreamer.Types.ChannelStat
import BStreamer.Types.VOD

type AccessToken = T.Text

logger :: String
logger = "bstreamer.twitch"

statUrl :: T.Text
statUrl = "https://api.twitch.tv/kraken/streams/"

twitchHeaders :: T.Text -> [RequestHeader]
twitchHeaders clientId = [("Accept", "application/vnd.twitchtv.v3+json"), ("Client-ID", encodeUtf8 clientId)]

getChannelJson :: T.Text -> Channel -> IO (Either BSError BL.ByteString)
getChannelJson clientId channel = httpGet (T.append statUrl channel) (twitchHeaders clientId)

getChannelStat :: TwitchConf -> Channel -> IO (Maybe ChannelStat)
getChannelStat conf chan =
  getChannelJson (twClientId conf) chan >>= process
  where process (Left err) = do
          logErrorL logger err ["getting stat for ", chan]
          return Nothing
        process (Right res) = do
          ts <- getCurrentTime
          return $ ChannelStat chan ts <$> decode res

oauthUrl :: T.Text
oauthUrl = "https://api.twitch.tv/kraken/oauth2/authorize"

getTwitchOAuth :: TwitchConf -> T.Text
getTwitchOAuth conf = T.append oauthUrl $ decodeUtf8 $ renderSimpleQuery True [
                         ("response_type", "code")
                       , ("client_id", encodeUtf8 (twClientId conf))
                       , ("redirect_uri", encodeUtf8 (twRedirectURL conf))
                       , ("scope", "user_read")
                       ]

tokenUrl :: T.Text
tokenUrl = "https://api.twitch.tv/kraken/oauth2/token"

data Token = Token {
    tokenAccessToken :: T.Text
} deriving (Generic, Show)

instance FromJSON Token where
  parseJSON = genericParseJSON $ aesonPrefix snakeCase

getAccessToken :: TwitchConf -> T.Text -> IO (Maybe AccessToken)
getAccessToken conf code = do
  eRes <- httpPost tokenUrl tokenBody []
  case eRes of
    Left err -> do
      logDebugL logger ["Error while getting access token: ", T.pack (show err)]
      return Nothing
    Right res -> return $ fmap tokenAccessToken (decode res)
  where tokenBody = [ ("client_id", encodeUtf8 (twClientId conf))
                    , ("client_secret", encodeUtf8 (twClientSecret conf))
                    , ("grant_type", "authorization_code")
                    , ("redirect_uri", encodeUtf8 (twRedirectURL conf))
                    , ("code", encodeUtf8 code)
                    ]

userUrl :: T.Text
userUrl = "https://api.twitch.tv/kraken/user"

data User = User {
    userName :: T.Text
} deriving (Generic, Show)

instance FromJSON User where
  parseJSON = genericParseJSON $ aesonPrefix snakeCase

getChannelName :: AccessToken -> IO (Maybe Channel)
getChannelName token = do
  eRes <- httpGet userUrl headers
  case eRes of
    Left err -> do
      logDebugL logger ["Error while getting user name: ", T.pack (show err)]
      return Nothing
    Right res -> return $ fmap userName (decode res)
  where headers = [ ("Accept", "application/vnd.twitchtv.v3+json")
                  , ("Authorization", encodeUtf8 $ T.append "OAuth " token)
                  ]

vodUrl :: Channel -> Int -> Int -> T.Text
vodUrl chan limit offset = T.concat [ "https://api.twitch.tv/kraken/channels/"
                                    , chan
                                    , "/videos?broadcasts=true&limit="
                                    , T.pack (show limit)
                                    , "&offset="
                                    , T.pack (show offset)
                                    ]

getVODJson :: T.Text -> Channel -> Int -> Int -> IO (Either BSError BL.ByteString)
getVODJson clientId channel limit offset = httpGet (vodUrl channel limit offset) (twitchHeaders clientId)

data VODList = VODList {
    vVods :: [VOD]
}

instance FromJSON VODList where
  parseJSON = withObject "obj" $ \o -> do
    vods <- o .: "videos"
    return $ VODList vods

getChannelVods :: TwitchConf -> Channel -> UTCTime -> UTCTime -> IO [VOD]
getChannelVods conf chan begin end = filter filterPred <$> accumulate 0
  where accumulate offset = do
          mVods <- getVods 50 offset
          case mVods of
            Nothing -> return []
            Just vods
              | contain vods -> if needMore vods
                                then fmap (vods++) (accumulate (offset + 50))
                                else return vods
              | checkMore vods -> accumulate (offset + 50)
              | otherwise -> return []
        getVods limit offset = getVODJson (twClientId conf) chan limit offset >>= process
        process (Left err) = do
          logErrorL logger err ["getting vods for ", chan]
          return Nothing
        process (Right res) = return $ vVods <$> decode res
        contain [] = False
        contain vods = vStart (last vods) <= end && vEnd (head vods) >= begin
        needMore [] = False
        needMore vods = vStart (last vods) > begin
        checkMore [] = False
        checkMore vods = vStart (last vods) > end
        filterPred vod = vStart vod <= end && vEnd vod >= begin
