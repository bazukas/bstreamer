module BStreamer.Http
( RequestHeader
, httpGet
, httpPost
) where

import Control.Arrow (left)
import Network.HTTP.Simple
import Network.HTTP.Types.Header (HeaderName)
import qualified Control.Exception as E
import qualified Data.ByteString as BS
import qualified Data.ByteString.Lazy as BL
import qualified Data.Text as T

import BStreamer.Types

type RequestHeader = (HeaderName, BS.ByteString)
type BodyParam = (BS.ByteString, BS.ByteString)

httpGet :: T.Text -> [RequestHeader] -> IO (Either BSError BL.ByteString)
httpGet url headers = left (HttpError . show) <$> tryBS
  where tryBS = E.try getBS :: IO (Either HttpException BL.ByteString)
        getBS = do
          request <- setRequestHeaders headers <$> parseRequest (T.unpack url)
          response <- httpLBS request
          return $ getResponseBody response

httpPost :: T.Text -> [BodyParam] -> [RequestHeader] -> IO (Either BSError BL.ByteString)
httpPost url body headers = left (HttpError . show) <$> tryBS
  where tryBS = E.try getBS :: IO (Either HttpException BL.ByteString)
        getBS = do
          request <- setRequestBodyURLEncoded body . setRequestHeaders headers <$> parseRequest ("POST " ++ T.unpack url)
          response <- httpLBS request
          return $ getResponseBody response
