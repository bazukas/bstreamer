module BStreamer.Types.User
( User(..)
, UserRole(..)
, getUser
, getAllUsers
, isAdmin
) where

import Data.Bson as B (Field(..), Val(..), Value(..), val, lookup, (=:))
import Data.Text (Text)

import BStreamer.DataStorage
import BStreamer.Types

data UserRole = RUser | RAdmin
  deriving (Read, Show, Eq)

instance Val UserRole where
  val RUser = Int32 0
  val RAdmin = Int32 1
  cast' (Int32 0) = Just RUser
  cast' (Int32 1) = Just RAdmin
  cast' _ = Nothing

data User = User {
    uChan :: Channel
  , uRole :: UserRole
} deriving (Eq, Read, Show)

instance Val User where
  val (User chan ur) = Doc
    [ "channel" := val chan
    , "role" := val ur
    ]
  cast' (Doc doc) = do
    chan <- B.lookup "channel" doc
    ur <- B.lookup "role" doc
    return $ User chan ur
  cast' _ = Nothing

userCollection :: Text
userCollection = "user"

getUser :: Channel -> DSConn -> IO (Maybe User)
getUser chan conn = get <$> getDocuments userCollection ["channel" =: chan] conn
  where get [] = Nothing
        get (u:_) = u

getAllUsers :: DSConn -> IO (Maybe [User])
getAllUsers conn = sequence <$> getDocuments userCollection [] conn

isAdmin :: User -> Bool
isAdmin (User _ RUser) = False
isAdmin (User _ RAdmin) = True
