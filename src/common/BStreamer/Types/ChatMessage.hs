module BStreamer.Types.ChatMessage
( ChatMessage(..)
, Emote(..)
, Badge(..)
, insertCM
, getChatMessages
, parseBadge
, parseEmote
) where

import Data.Aeson as A (ToJSON(..), Value(..), object, (.=))
import Data.Bson as B (Field(..), Val(..), Value(..), val, lookup, (=:))
import Data.Maybe (fromMaybe)
import Data.Text (Text, splitOn)
import Data.Time.Clock (UTCTime)

import BStreamer.DataStorage
import BStreamer.Types

data Badge = BadgeStaff
           | BadgeAdmin
           | BadgeGlobalMod
           | BadgeModerator
           | BadgeSubscriber
           | BadgeTurbo
           | BadgePremium deriving (Eq, Show)

data Emote = Emote {
    eId :: Text
  , eIndexes :: Text
} deriving (Eq, Show)

data ChatMessage = ChatMessage {
    cmChannel   :: Channel
  , cmTimestamp :: UTCTime
  , cmUsername  :: Channel
  , cmMessage   :: Text
  , cmColor     :: Maybe Text
  , cmBadges    :: [Badge]
  , cmEmotes    :: [Emote]
} deriving (Eq, Show)

instance Val Badge where
  val BadgeStaff = B.String "staff"
  val BadgeAdmin = B.String "admin"
  val BadgeGlobalMod = B.String "globalmod"
  val BadgeModerator = B.String "mod"
  val BadgeSubscriber = B.String "subscriber"
  val BadgeTurbo = B.String "turbo"
  val BadgePremium = B.String "premium"
  cast' (B.String "staff") = Just BadgeStaff
  cast' (B.String "admin") = Just BadgeAdmin
  cast' (B.String "globalmod") = Just BadgeGlobalMod
  cast' (B.String "mod") = Just BadgeModerator
  cast' (B.String "subscriber") = Just BadgeSubscriber
  cast' (B.String "turbo") = Just BadgeTurbo
  cast' (B.String "premium") = Just BadgePremium
  cast' _ = Nothing

instance Val Emote where
  val (Emote eid indexes) = Doc
    [ "id" := val eid
    , "indexes" := val indexes
    ]
  cast' (Doc doc) = do
    eid <- B.lookup "id" doc
    indexes <- B.lookup "indexes" doc
    return $ Emote eid indexes
  cast' _ = Nothing

instance Val ChatMessage where
  val (ChatMessage ch ts user msg color badges emotes) = Doc
    [ "channel" := val ch
    , "timestamp" := val ts
    , "user" := val user
    , "msg" := val msg
    , "color" := valMaybe color
    , "badges" := val badges
    , "emotes" := val emotes
    ]
  cast' (Doc doc) = do
    chan <- B.lookup "channel" doc
    ts <- B.lookup "timestamp" doc
    user <- B.lookup "user" doc
    msg <- B.lookup "msg" doc
    let color = B.lookup "color" doc
    let badges = fromMaybe [] $ B.lookup "badges" doc
    let emotes = fromMaybe [] $ B.lookup "emotes" doc
    return $ ChatMessage chan ts user msg color badges emotes
  cast' _ = Nothing

instance ToJSON Badge where
  toJSON BadgeStaff = A.String "staff"
  toJSON BadgeAdmin = A.String "admin"
  toJSON BadgeGlobalMod = A.String "globalmod"
  toJSON BadgeModerator = A.String "mod"
  toJSON BadgeSubscriber = A.String "subscriber"
  toJSON BadgeTurbo = A.String "turbo"
  toJSON BadgePremium = A.String "premium"

instance ToJSON Emote where
  toJSON (Emote eid indexes) =
    object [ "id" .= eid
           , "indexes" .= indexes
           ]

instance ToJSON ChatMessage where
  toJSON (ChatMessage ch ts user msg color badges emotes) =
    object [ "channel" .= ch
           , "timestamp" .= ts
           , "user" .= user
           , "msg" .= msg
           , "color" .= color
           , "badges" .= badges
           , "emotes" .= emotes
           ]

chatCollection :: Text
chatCollection = "chat_message"

insertCM :: ChatMessage -> DSConn -> IO ()
insertCM = insertDocument_ chatCollection

getChatMessages :: Channel -> UTCTime -> UTCTime -> DSConn -> IO (Maybe [ChatMessage])
getChatMessages chan start end conn = sequence <$> getDocumentsLO chatCollection docFilter 0 0 sort conn
  where docFilter = [ "channel" =: chan
                 , "timestamp" =: ("$gte" =: start)
                 , "timestamp" =: ("$lte" =: end)
                 ]
        sort = [ "timestamp" =: (1 :: Int) ]

parseBadge :: Text -> Maybe Badge
parseBadge "staff" = Just BadgeStaff
parseBadge "admin" = Just BadgeAdmin
parseBadge "global_mod" = Just BadgeGlobalMod
parseBadge "moderator" = Just BadgeModerator
parseBadge "subscriber" = Just BadgeSubscriber
parseBadge "turbo" = Just BadgeTurbo
parseBadge "premium" = Just BadgePremium
parseBadge _ = Nothing

parseEmote :: Text -> Maybe Emote
parseEmote t = case splitOn ":" t of
                [eid,indexes] -> Just $ Emote eid indexes
                _ -> Nothing
