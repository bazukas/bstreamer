module BStreamer.Types.StreamEvent
( StreamEvent(..)
) where

import Data.Aeson as A (ToJSON(..), Value(..), object, (.=))
import Data.Bson as B (Field(..), Val(..), Value(..), val, lookup)
import Data.Text (Text)

data StreamEvent =
    SubscriberOnlyOn
  | SubscriberOnlyOff
  | SlowMode Int
  | SlowModeOff
  | R9KOn
  | R9KOff
  | EmoteOnlyOn
  | EmoteOnlyOff
  | ClearChat
  | Ban Text Text
  | TimeOut Text Int Text
  | GameChange Text
  | StatusChange Text
  deriving (Eq, Ord, Show)

instance ToJSON StreamEvent where
  toJSON SubscriberOnlyOn = A.String "subs_on"
  toJSON SubscriberOnlyOff = A.String "subs_off"
  toJSON (SlowMode s) =
    object [ "event" .= ("slow_on" :: Text)
           , "seconds" .= s
           ]
  toJSON SlowModeOff = A.String "slow_off"
  toJSON R9KOn = A.String "r9k_on"
  toJSON R9KOff = A.String "r9k_off"
  toJSON EmoteOnlyOn = A.String "emote_only_on"
  toJSON EmoteOnlyOff = A.String "emote_only_off"
  toJSON ClearChat = A.String "clear_chat"
  toJSON (Ban u r) =
    object [ "event" .= ("ban" :: Text)
           , "user" .= u
           , "reason" .= r
           ]
  toJSON (TimeOut u d r) =
    object [ "event" .= ("timeout" :: Text)
           , "user" .= u
           , "duration" .= d
           , "reason" .= r
           ]
  toJSON (GameChange t) =
    object [ "event" .= ("game_change" :: Text)
           , "game" .= t
           ]
  toJSON (StatusChange t) =
    object [ "event" .= ("status_change" :: Text)
           , "status" .= t
           ]

instance Val StreamEvent where
  val SubscriberOnlyOn = B.String "subs_on"
  val SubscriberOnlyOff = B.String "subs_off"
  val (SlowMode s) = Doc
    [ "event" := B.String "slow_on"
    , "seconds" := B.Int32 (fromIntegral s)
    ]
  val SlowModeOff = B.String "slow_off"
  val R9KOn = B.String "r9k_on"
  val R9KOff = B.String "r9k_off"
  val EmoteOnlyOn = B.String "emote_only_on"
  val EmoteOnlyOff = B.String "emote_only_off"
  val ClearChat = B.String "clear_chat"
  val (Ban u r) = Doc
    [ "event" := B.String "ban"
    , "user" := B.String u
    , "reason" := B.String r
    ]
  val (TimeOut u d r) = Doc
    [ "event" := B.String "timeout"
    , "user" := B.String u
    , "duration" := B.Int32 (fromIntegral d)
    , "reason" := B.String r
    ]
  val (GameChange t) = Doc
    [ "event" := B.String "game_change"
    , "game" := B.String t
    ]
  val (StatusChange t) = Doc
    [ "event" := B.String "status_change"
    , "status" := B.String t
    ]
  cast' (B.String "subs_on") = Just SubscriberOnlyOn
  cast' (B.String "subs_off") = Just SubscriberOnlyOff
  cast' (B.String "slow_off") = Just SlowModeOff
  cast' (B.String "r9k_on") = Just R9KOn
  cast' (B.String "r9k_off") = Just R9KOff
  cast' (B.String "emote_only_on") = Just EmoteOnlyOn
  cast' (B.String "emote_only_off") = Just EmoteOnlyOff
  cast' (B.String "clear_chat") = Just ClearChat
  cast' (Doc doc) = do
    event <- B.lookup "event" doc :: Maybe Text
    case event of
      "slow_on" -> B.lookup "seconds" doc >>= (Just . SlowMode)
      "ban" -> do
        u <- B.lookup "user" doc
        r <- B.lookup "reason" doc
        return $ Ban u r
      "timeout" -> do
        u <- B.lookup "user" doc
        d <- B.lookup "duration" doc
        r <- B.lookup "reason" doc
        return $ TimeOut u d r
      "game_change" -> B.lookup "game" doc >>= (Just . GameChange)
      "status_change" -> B.lookup "status" doc >>= (Just . StatusChange)
      _ -> Nothing
  cast' _ = Nothing
