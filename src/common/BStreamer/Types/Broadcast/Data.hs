module BStreamer.Types.Broadcast.Data
( BroadcastData(..)
, GraphData(..)
, EventData(..)
, WordCloud(..)
, Reaction(..)
) where

import Control.Monad (liftM2)
import Data.Aeson (ToJSON(..), object, (.=))
import Data.Bson as B (Val(..), Value(..), Field(..), lookup)
import Data.Maybe (fromMaybe)
import Data.Time.Clock (UTCTime)
import qualified Data.Map.Strict as Map
import qualified Data.Text as T

import BStreamer.Types.StreamEvent

data GraphData = GraphData {
    gdTimestamp      :: UTCTime
  , gdViewers        :: Int
  , gdFollowers      :: Int
  , gdChatActivity   :: Int
  , gdModActivity    :: Int
  , gdLurkerActivity :: Int
} deriving (Eq, Show)

data EventData = EventData {
    edTimestamp :: UTCTime
  , edEvent     :: StreamEvent
} deriving (Eq, Ord, Show)

data WordCloud = WordCloud {
    wcStart :: UTCTime
  , wcEnd :: UTCTime
  , wcWordMap :: Map.Map T.Text Int
} deriving (Eq, Ord, Show)

data Reaction = Reaction {
    rReaction :: T.Text
  , rTimestamp :: UTCTime
  , rSize :: Int
} deriving (Eq, Ord, Show)

data BroadcastData = BroadcastData {
    bdGraphData :: [GraphData]
  , bdEventData :: [EventData]
  , bdWordCloud :: [WordCloud]
  , bdReactions :: [Reaction]
  , bdTitle :: T.Text
  , bdAvgViewers :: Float
  , bdAvgChatActivity :: Float
  , bdFollowersGained :: Int
} deriving (Eq, Show)

instance ToJSON GraphData where
  toJSON (GraphData ts v f ca ma la) =
    object [ "timestamp" .= ts
           , "viewers" .= v
           , "followers" .= f
           , "chat_activity" .= ca
           , "mod_activity" .= ma
           , "lurker_activity" .= la
           ]

instance ToJSON EventData where
  toJSON (EventData ts event) =
    object [ "timestamp" .= ts
           , "event" .= event
           ]

instance ToJSON WordCloud where
  toJSON (WordCloud st ed wm) =
    object [ "start" .= st
           , "end" .= ed
           , "word_map" .= wm
           ]

instance ToJSON Reaction where
  toJSON (Reaction r ts s) =
    object [ "reaction" .= r
           , "timestamp" .= ts
           , "size" .= s
           ]

instance ToJSON BroadcastData where
  toJSON (BroadcastData gd ed wc rs t v ca f) =
    object [ "graph_data" .= gd
           , "event_data" .= ed
           , "word_cloud" .= wc
           , "reactions" .= rs
           , "title" .= t
           , "avg_viewers" .= v
           , "avg_chat_activity" .= ca
           , "followers_gained" .= f
           ]

instance Val GraphData where
  val (GraphData ts v f ca ma la) = Doc
    [ "timestamp" := val ts
    , "viewers" := val v
    , "followers" := val f
    , "chat_activity" := val ca
    , "mod_activity" := val ma
    , "lurker_activity" := val la
    ]
  cast' (Doc doc) = do
    ts <- B.lookup "timestamp" doc
    viewers <- B.lookup "viewers" doc
    followers <- B.lookup "followers" doc
    chat_activity <- B.lookup "chat_activity" doc
    let mod_activity = fromMaybe 0 (B.lookup "mod_activity" doc)
    let lurker_activity = fromMaybe 0 (B.lookup "lurker_activity" doc)
    return $ GraphData ts viewers followers chat_activity mod_activity lurker_activity
  cast' _ = Nothing

instance Val EventData where
  val (EventData ts event) = Doc
    [ "timestamp" := val ts
    , "event" := val event
    ]
  cast' (Doc doc) = do
    ts <- B.lookup "timestamp" doc
    event <- B.lookup "event" doc
    return $ EventData ts event
  cast' _ = Nothing

instance Val WordCloud where
  val (WordCloud st ed wm) = Doc
    [ "start" := val st
    , "end" := val ed
    , "word_map" := val ((\(w,c) -> Doc [ "word" := val w, "count" := val c ]) <$> Map.toList wm)
    ]
  cast' (Doc doc) = do
    st <- B.lookup "start" doc
    ed <- B.lookup "end" doc
    wm <- B.lookup "word_map" doc
    let castPair (Doc doc') = liftM2 (,) (B.lookup "word" doc') (B.lookup "count" doc')
        castPair _ = Nothing
    wm' <- sequence $ fmap castPair wm
    return $ WordCloud st ed (Map.fromList wm')
  cast' _ = Nothing

instance Val Reaction where
  val (Reaction r ts s) = Doc
    [ "reaction" := val r
    , "timestamp" := val ts
    , "size" := val s
    ]
  cast' (Doc doc) = do
    r <- B.lookup "reaction" doc
    ts <- B.lookup "timestamp" doc
    s <- B.lookup "size" doc
    return $ Reaction r ts s
  cast' _ = Nothing

instance Val BroadcastData where
  val (BroadcastData gd ed wc rs t v ca f) = Doc
    [ "graph_data" := val gd
    , "event_data" := val ed
    , "word_cloud" := val wc
    , "reactions" := val rs
    , "title" := val t
    , "avg_viewers" := val v
    , "avg_chat_activity" := val ca
    , "followers_gained" := val f
    ]
  cast' (Doc doc) = do
    gd <- B.lookup "graph_data" doc
    ed <- B.lookup "event_data" doc
    wc <- B.lookup "word_cloud" doc
    rs <- B.lookup "reactions" doc
    title <- B.lookup "title" doc
    avgViewers <- B.lookup "avg_viewers" doc
    avgChatActivity <- B.lookup "avg_chat_activity" doc
    followersGained <- B.lookup "followers_gained" doc
    return $ BroadcastData gd ed wc rs title avgViewers avgChatActivity followersGained
  cast' _ = Nothing
