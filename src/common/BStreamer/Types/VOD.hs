module BStreamer.Types.VOD
( VOD(..)
) where

import Data.Aeson (FromJSON, parseJSON, withObject, (.:), ToJSON(..), object, (.=))
import Data.Text (Text)
import Data.Time.Clock (UTCTime, addUTCTime)

import BStreamer.Types

data VOD = VOD {
    vChannel :: Channel
  , vStart   :: UTCTime
  , vEnd     :: UTCTime
  , vURL     :: Text
} deriving Show

instance FromJSON VOD where
  parseJSON = withObject "obj" $ \o -> do
    channel <- o .: "channel"
    channelName <- channel .: "name"
    url <- o .: "url"
    start <- o .: "recorded_at"
    len <- o .: "length"
    return $ VOD channelName start (addUTCTime len start) url

instance ToJSON VOD where
  toJSON (VOD chan start end url) =
    object [ "channel" .= chan
           , "start" .= start
           , "end" .= end
           , "url" .= url
           ]
