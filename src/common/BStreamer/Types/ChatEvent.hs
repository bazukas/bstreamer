module BStreamer.Types.ChatEvent
( ChatEvent(..)
, insertCE
, getChatEvents
) where

import Data.Bson as B (Field(..), Val(..), Value(..), val, lookup, (=:))
import Data.Maybe (catMaybes)
import Data.Text (Text)
import Data.Time.Clock (UTCTime)

import BStreamer.DataStorage
import BStreamer.Types
import BStreamer.Types.StreamEvent

data ChatEvent = ChatEvent {
    ceChannel   :: Channel
  , ceTimestamp :: UTCTime
  , ceEvent     :: StreamEvent
} deriving (Eq, Show)

instance Val ChatEvent where
  val (ChatEvent chan ts e) = Doc
    [ "channel" := val chan
    , "timestamp" := val ts
    , "event" := val e
    ]
  cast' (Doc doc) = do
    chan <- B.lookup "channel" doc
    timestamp <- B.lookup "timestamp" doc
    event <- B.lookup "event" doc
    return $ ChatEvent chan timestamp event
  cast' _ = Nothing

chatEventCollection :: Text
chatEventCollection = "chat_event"

insertCE :: ChatEvent -> DSConn -> IO ()
insertCE = insertDocument_ chatEventCollection

getChatEvents :: Channel -> UTCTime -> UTCTime -> DSConn -> IO [ChatEvent]
getChatEvents chan start end conn = catMaybes <$> getDocumentsLO chatEventCollection docFilter 0 0 sort conn
  where docFilter = [ "channel" =: chan
                 , "timestamp" =: ("$gte" =: start)
                 , "timestamp" =: ("$lte" =: end)
                 ]
        sort = [ "timestamp" =: (1 :: Int) ]
