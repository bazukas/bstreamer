module BStreamer.Types.Broadcast
( AnalyzeStatus(..)
, Broadcast(..)
, newBroadcast
, saveBroadcast
, getBroadcast
, getChannelBroadcasts
, getChannelBroadcastCount
, getBroadcasts
, getBroadcastCount
) where

import Data.Bson as B (Field(..), Val(..), Value(..), ObjectId, val, valMaybe, lookup, (=:), genObjectId)
import Data.Text (Text, pack, unpack)
import Data.Time.Clock (UTCTime)
import Text.Read (readMaybe)

import BStreamer.DataStorage
import BStreamer.Types
import BStreamer.Types.Broadcast.Data
import BStreamer.Types.ChannelStat

data AnalyzeStatus = Scheduled | InProgress | Success | Error
  deriving (Eq, Show)

instance Val AnalyzeStatus where
  val Scheduled = Int32 0
  val InProgress = Int32 1
  val Success = Int32 2
  val Error = Int32 3
  cast' (Int32 0) = Just Scheduled
  cast' (Int32 1) = Just InProgress
  cast' (Int32 2) = Just Success
  cast' (Int32 3) = Just Error
  cast' _ = Nothing

data Broadcast = Broadcast {
    bcId        :: Text
  , bcChannel   :: Channel
  , bcDateStart :: UTCTime
  , bcDateEnd   :: UTCTime
  , bcStats     :: [ChannelStat]
  , bcData      :: Maybe BroadcastData
  , bcStatus    :: AnalyzeStatus
} deriving (Eq, Show)

instance Val Broadcast where
  val (Broadcast bcid chan ds de stats bcdata status) = Doc
    [ "_id" := val (read . unpack $ bcid :: ObjectId)
    , "channel" := val chan
    , "date_start" := val ds
    , "date_end" := val de
    , "stats" := val stats
    , "data" := valMaybe bcdata
    , "status" := val status
    ]
  cast' (Doc doc) = do
    bcid <- B.lookup "_id" doc :: Maybe ObjectId
    chan <- B.lookup "channel" doc
    ds <- B.lookup "date_start" doc
    de <- B.lookup "date_end" doc
    stats <- B.lookup "stats" doc
    bcdata <- B.lookup "data" doc
    status <- B.lookup "status" doc
    return $ Broadcast (pack . show $ bcid) chan ds de stats bcdata status
  cast' _ = Nothing

broadcastCollection :: Text
broadcastCollection = "broadcast"

newBroadcast :: Channel -> UTCTime -> UTCTime -> [ChannelStat] -> DSConn -> IO (Maybe Text)
newBroadcast chan ds de stats conn = do
  oid <- genObjectId
  getid <$> insertDocument broadcastCollection (broadcast oid) conn
  where broadcast oid = Broadcast (pack . show $ oid) chan ds de stats Nothing Scheduled
        getid (ObjId oid) = Just (pack . show $ oid)
        getid _ = Nothing

saveBroadcast :: Broadcast -> DSConn -> IO ()
saveBroadcast br = upsertDocument broadcastCollection ["_id" =: (read . unpack $ bcId br :: ObjectId)] br

getBroadcast :: Text -> DSConn -> IO (Maybe Broadcast)
getBroadcast oid conn =
  case moid of
    Nothing -> return Nothing
    Just oid' -> get <$> getDocuments broadcastCollection ["_id" =: oid'] conn
  where moid = readMaybe . unpack $ oid :: Maybe ObjectId
        get [] = Nothing
        get (x:_) = x

getChannelBroadcasts :: Channel -> Int -> Int -> DSConn -> IO (Maybe [Broadcast])
getChannelBroadcasts chan limit offset conn =
  sequence <$> getDocumentsLO broadcastCollection
                              ["channel" =: chan]
                              limit
                              offset
                              ["date_end" =: (-1 :: Int)]
                              conn

getChannelBroadcastCount :: Channel -> DSConn -> IO Int
getChannelBroadcastCount chan = countDocuments broadcastCollection ["channel" =: chan]

getBroadcasts :: Int -> Int -> DSConn -> IO (Maybe [Broadcast])
getBroadcasts limit offset conn =
  sequence <$> getDocumentsLO broadcastCollection
                              []
                              limit
                              offset
                              ["date_end" =: (-1 :: Int)]
                              conn

getBroadcastCount :: DSConn -> IO Int
getBroadcastCount = countDocuments broadcastCollection []
