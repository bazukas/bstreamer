{-# LANGUAGE ExtendedDefaultRules #-}
module BStreamer.Types.ChannelStat
( Stat(..)
, ChannelStat(..)
, insertStat
, deleteChannelStats
, getLastTimestamp
, getCurChannels
) where

import Data.Aeson (FromJSON, parseJSON, withObject, (.:), (.:?))
import Data.Bson as B (Field(..), Val(..), Value(..), val, (=:), lookup)
import Data.Maybe (fromMaybe)
import Data.Time.Clock (UTCTime)
import qualified Data.Text as T

import BStreamer.DataStorage
import BStreamer.Types

data Stat = Stat {
    stViewers   :: Int
  , stFollowers :: Int
  , stGame      :: T.Text
  , stStatus    :: T.Text
} deriving (Show, Eq, Ord)

data ChannelStat = ChannelStat {
    csChannel   :: Channel
  , csTimestamp :: UTCTime
  , csStat      :: Stat
} deriving (Show, Eq, Ord)

instance FromJSON Stat where
  parseJSON = withObject "obj" $ \o -> do
    stream <- o .: "stream"
    viewers <- stream .: "viewers"
    channel <- stream .: "channel"
    game <- stream .:? "game"
    followers <- channel .: "followers"
    status <- channel .:? "status"
    return $ Stat viewers followers (fromMaybe "" game) (fromMaybe "" status)

instance Val ChannelStat where
  val (ChannelStat ch ts (Stat v f g s)) = Doc
    [ "channel" := val ch
    , "timestamp" := val ts
    , "viewers" := val v
    , "followers" := val f
    , "game" := val g
    , "status" := val s
    ]
  cast' (Doc doc) = do
    chan <- B.lookup "channel" doc
    ts <- B.lookup "timestamp" doc
    viewers <- B.lookup "viewers" doc
    followers <- B.lookup "followers" doc
    game <- B.lookup "game" doc
    status <- B.lookup "status" doc
    return $ ChannelStat chan ts (Stat viewers followers game status)
  cast' _ = Nothing

statCollection :: T.Text
statCollection = "channel_stat"

insertStat :: ChannelStat -> DSConn -> IO ()
insertStat = insertDocument_ statCollection

deleteChannelStats :: Channel -> DSConn -> IO (Maybe [ChannelStat])
deleteChannelStats chan conn = do
  stats <- getDocuments statCollection docFilter conn
  deleteDocuments statCollection docFilter conn
  return $ sequence stats
  where docFilter = ["channel" =: chan]

getLastTimestamp :: Channel -> DSConn -> IO (Maybe UTCTime)
getLastTimestamp chan conn = getMaxTs <$> getAggregate statCollection pipeline conn
  where pipeline = [ ["$match" =: ("channel" =: chan)]
                   , ["$group" =: [ "_id" =: Null
                                  , "timestamp" =: ("$max" =: ("$timestamp" :: String))
                                  ]
                     ]
                   ]
        getMaxTs [] = Nothing
        getMaxTs (doc:_) = B.lookup "timestamp" doc

getCurChannels :: DSConn -> IO (Maybe [Channel])
getCurChannels conn = sequence <$> distinctValues statCollection [] "channel" conn
