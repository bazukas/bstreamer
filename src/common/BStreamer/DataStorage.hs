module BStreamer.DataStorage
( DSConn(..)
, connectDS
, disconnectDS
, insertDocument
, insertDocument_
, getAggregate
, deleteDocuments
, getDocuments
, getDocumentsLO
, upsertDocument
, countDocuments
, distinctValues
) where

import Control.Arrow (left)
import Control.Exception (try)
import Control.Monad (void)
import Database.MongoDB
import Data.Text (Text, unpack)

import BStreamer.Config
import BStreamer.Log
import BStreamer.Types

logger :: String
logger = "bstreamer.datastorage"

data DSConn = DSConn {
    dsPipe :: Pipe
  , dsDatabase :: Text
}

connectDS :: DSConf -> IO (Either BSError DSConn)
connectDS conf = do
  logInfo logger "Connecting to mongoDB"
  pipe <- try (connect (host (unpack $ dsHost conf))) :: IO (Either IOError Pipe)
  let econn = (`DSConn` dsDB conf) <$> pipe
  return $ left (DbError . show) econn

disconnectDS :: DSConn -> IO ()
disconnectDS conn = do
  logInfo logger "Disconnecting from mongoDB"
  close (dsPipe conn)

runAction :: DSConn -> Action IO a -> IO a
runAction conn = access (dsPipe conn) master (dsDatabase conn)

insertDocument :: Val a => Collection -> a -> DSConn -> IO Value
insertDocument col obj conn =
  case val obj of
    Doc doc -> runAction conn $ insert col doc
    _ -> return Null

insertDocument_ :: Val a => Collection -> a -> DSConn -> IO ()
insertDocument_ col obj conn = void $ insertDocument col obj conn

upsertDocument :: Val a => Collection -> Document -> a -> DSConn -> IO ()
upsertDocument col sel obj conn =
  case val obj of
    Doc doc -> runAction conn $ upsert (select sel col) doc
    _ -> return ()

getAggregate :: Collection -> Pipeline -> DSConn -> IO [Document]
getAggregate col pl conn = runAction conn $ aggregate col pl

getDocuments :: Val a => Collection -> Document -> DSConn -> IO [Maybe a]
getDocuments col doc conn = (fmap . fmap) (cast . Doc) $ runAction conn $ rest =<< find (select doc col)

getDocumentsLO :: Val a => Collection -> Document -> Int -> Int -> Document -> DSConn -> IO [Maybe a]
getDocumentsLO col doc lim sk sortd conn = (fmap . fmap) (cast . Doc) $ runAction conn $
  rest =<< find ((select doc col) { limit = fromIntegral lim, skip = fromIntegral sk, sort = sortd })

deleteDocuments :: Collection -> Document -> DSConn -> IO ()
deleteDocuments col doc conn = runAction conn $ delete (select doc col)

countDocuments :: Collection -> Document -> DSConn -> IO Int
countDocuments col doc conn = runAction conn $ count (select doc col)

distinctValues :: Val a => Collection -> Document -> Text -> DSConn -> IO [Maybe a]
distinctValues col doc lab conn = (fmap . fmap) cast $ runAction conn $ distinct lab (select doc col)
