module BStreamer.Config
( IrcConf(..)
, DSConf(..)
, ViewerConf(..)
, AnalyzerConf(..)
, WebConf(..)
, TwitchConf(..)
, AMQPConf(..)
, BSettings(..)
, readSettings
) where

import qualified Data.Configurator as C
import qualified Data.Text as T

import BStreamer.Log

data DSConf = DSConf {
    dsHost :: T.Text
  , dsDB :: T.Text
} deriving Show

data IrcConf = IrcConf {
    ircHost :: String
  , ircPort :: Int
  , ircUser :: T.Text
  , ircReconnectPeriod :: Int
  , ircRateLimitPenalty :: Rational
  , ircRateLimitThreshold :: Rational
  , ircDebug :: Bool
  , ircPingTimeout :: Int
} deriving Show

data ViewerConf = ViewerConf {
    vCheckPeriod        :: Int
  , vStatPeriod         :: Int
  , vStatRetries        :: Int
  , vRestorePeriod      :: Int
  , vThreadsCheckPeriod :: Int
} deriving Show

data AnalyzerConf = AnalyzerConf {
    aConsumersNum :: Int
} deriving Show

data WebConf = WebConf {
    wBroadcastsPerPage :: Int
} deriving Show

data TwitchConf = TwitchConf {
    twClientId     :: T.Text
  , twClientSecret :: T.Text
  , twRedirectURL  :: T.Text
} deriving Show

data AMQPConf = AMQPConf {
    amqpHost        :: T.Text
  , amqpVirtualHost :: T.Text
  , amqpLogin       :: T.Text
  , amqpPass        :: T.Text
} deriving Show

data BSettings = BSettings {
    bsDSConf       :: DSConf
  , bsIrcConf      :: IrcConf
  , bsViewerConf   :: ViewerConf
  , bsAnalyzerConf :: AnalyzerConf
  , bsWebConf      :: WebConf
  , bsTwitchConf   :: TwitchConf
  , bsAMQPConf     :: AMQPConf
} deriving Show

logger :: String
logger = "bstreamer.config"

readSettings :: IO BSettings
readSettings = do
  logInfo logger "Reading configuration"
  config <- C.load [C.Required "bstreamer.conf", C.Optional "bstreamer.local.conf"]
  dsConf <- getDSConf config
  ircConf <- getIrcConf config
  viewerConf <- getViewerConf config
  analyzerConf <- getAnalyzerConf config
  webConf <- getWebConf config
  twitchConf <- getTwitchConf config
  amqpConf <- getAMQPConf config
  return $ BSettings dsConf ircConf viewerConf analyzerConf webConf twitchConf amqpConf
  where
    getDSConf config = do
      host <- C.require config "ds.host"
      db <- C.require config "ds.db"
      return $ DSConf host db
    getIrcConf config = do
      host <- C.require config "irc.host"
      port <- C.require config "irc.port"
      user <- C.require config "irc.user"
      reconnectPeriod <- C.require config "irc.reconnectPeriod"
      penalty <- C.require config "irc.rateLimitPenalty"
      threshold <- C.require config "irc.rateLimitThreshold"
      debug <- C.require config "irc.debug"
      pingTimeout <- C.require config "irc.pingTimeout"
      return $ IrcConf host port user reconnectPeriod penalty threshold debug pingTimeout
    getViewerConf config = do
      checkPeriod <- C.require config "viewer.checkPeriod"
      statPeriod <- C.require config "viewer.statPeriod"
      statRetries <- C.require config "viewer.statRetries"
      restorePeriod <- C.require config "viewer.restorePeriod"
      threadsCheckPeriod <- C.require config "viewer.threadsCheckPeriod"
      return $ ViewerConf checkPeriod statPeriod statRetries restorePeriod threadsCheckPeriod
    getAnalyzerConf config = do
      consumersNum <- C.require config "analyzer.consumersNum"
      return $ AnalyzerConf consumersNum
    getWebConf config = do
      broadcastsPerPage <- C.require config "web.broadcastsPerPage"
      return $ WebConf broadcastsPerPage
    getTwitchConf config = do
      clientId <- C.require config "twitch.clientId"
      clientSecret <- C.require config "twitch.clientSecret"
      redirectURL <- C.require config "twitch.redirectURL"
      return $ TwitchConf clientId clientSecret redirectURL
    getAMQPConf config = do
      host <- C.require config "amqp.host"
      virtualHost <- C.require config "amqp.virtualHost"
      login <- C.require config "amqp.login"
      pass <- C.require config "amqp.pass"
      return $ AMQPConf host virtualHost login pass
