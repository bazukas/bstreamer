import { BaseType, Selection, select } from "d3-selection";
import { utcParse, timeFormat } from "d3-time-format";
import { json } from "d3-request";
import StayDown = require("staydown");

import { initSpinner } from "./spin";
import { PlayerApi } from "./types";

type BSelection = Selection<BaseType, {}, HTMLElement, any>;

interface Badge {
    alpha: string;
    image: string;
    svg: string;
}

interface BadgeMap {
    admin: Badge;
    broadcaster: Badge;
    global_mod: Badge;
    mod: Badge;
    staff: Badge;
    subscriber: Badge;
    turbo: Badge;
    [key: string]: Badge;
}

interface Emote {
    id: string;
    indexes: string;
}

interface RawChatMessage {
    badges: Array<string>;
    channel: string;
    color: string;
    emotes: Array<Emote>;
    msg: string;
    timestamp: string;
    user: string;
}

interface ChatMessage extends RawChatMessage {
    date: Date;
    ts: number;
}

interface ChatState {
    container: BSelection;
    containerId: string;
    channel: string;
    playerApi: PlayerApi;
    cache: Array<ChatMessage>;
    cstatus: { loading: boolean };
    lastCheckedTs: number;
    previousTs: number;
    userScrolling: boolean;
    badges: BadgeMap;
}

function loadBadges(chat: ChatState, twcid: string): void {
    json("https://api.twitch.tv/kraken/chat/" + chat.channel + "/badges?client_id=" + twcid,
         function(error: Error, data: BadgeMap): void {
            if (error) throw error;
            chat.badges = data;
    });
}

function dateToTimestamp(d: Date): number {
    return Math.floor(d.getTime() / 1000);
}

function loadChat(chat: ChatState, start: number, showSpinner: boolean): void {
    let cache = chat.cache;
    let cstatus = chat.cstatus;
    cstatus.loading = true;

    let parseTime = utcParse("%Y-%m-%dT%H:%M:%S.%LZ");
    let parseTime2 = utcParse("%Y-%m-%dT%H:%M:%SZ");
    let end = start + 30;
    chat.lastCheckedTs = end;

    let spinner: Spinner;
    if (showSpinner) {
        spinner = initSpinner(chat.containerId);
    }

    json("/chat/" + chat.channel + "/" + start + "/" + end, function(error: Error, data: Array<RawChatMessage>) {
        if (error) throw error;

        if (showSpinner) {
            spinner.stop();
        }

        let cms = data.map(function(o) {
            let date = parseTime(o.timestamp) || parseTime2(o.timestamp);
            let ts = dateToTimestamp(date);
            return {
                badges: o.badges,
                channel: o.channel,
                color: o.color,
                emotes: o.emotes,
                msg: o.msg,
                timestamp: o.timestamp,
                user: o.user,
                date: date,
                ts: ts
            }
        });

        Array.prototype.push.apply(cache, cms);

        cstatus.loading = false;
    });
}

function clearChat(chat: ChatState): ChatState {
    chat.container.html("");
    chat.cache = [];
    chat.cstatus = { loading: false };
    chat.lastCheckedTs = null;
    chat.previousTs = null;
    return chat;
}

function replaceTwitchEmotes(text: string, emotes: Array<Emote>): string {
    let emotesToReplace: Array<{ id: string, begin: number, end: number }> = [];
    let baseUrl = "http://static-cdn.jtvnw.net/emoticons/v1/";

    for (let i = 0; i < emotes.length; ++i) {
        let emote = emotes[i];
        let indexes = emote.indexes.split(",");
        for (let j = 0; j < indexes.length; ++j) {
            let index = indexes[j].split("-");
            emotesToReplace.push({ id: emote.id, begin: parseInt(index[0]), end: parseInt(index[1]) })
        }
    }

    emotesToReplace.sort(function(x, y) {
        return y.begin - x.begin;
    });

    let messageParts: Array<string | string[]> = [];

    emotesToReplace.forEach(function(emote) {
        let emoteText = text.substring(emote.begin, emote.end + 1);
        let emoteUrl = baseUrl + emote.id + "/1.0";
        messageParts.unshift(text.slice(emote.end + 1));
        messageParts.unshift([buildEmote(emoteUrl, emoteText)]);
        text = text.slice(0, emote.begin);
    });

    messageParts.unshift(text);

    function buildEmote(url: string, text: string): string {
        return "<img class='chatemote' src='" + url + "' alt='" + text + "' title='" + text + "'/>"
    }

    function escapeHtml(text: string): string {
        return text.replace(/&/g, '&amp;').replace(/</g,'&lt;').replace(/>/g, '&gt;');
    }

    let newText = "";
    messageParts.forEach(function(part) {
        part = Array.isArray(part) ? part[0] : escapeHtml(part);
        newText += part;
    });

    return messageParts.join('');
};

function appendMessage(container: BSelection, cm: ChatMessage, twbadges: BadgeMap) {
    let line = container.append("div")
        .attr("class", "chatline");
    let badges = line.append("span")
        .attr("class", "chatbadges");
    if (twbadges) {
        for (let i = 0; i < cm.badges.length; ++i) {
            let b = cm.badges[i];
            if (b in twbadges) {
                badges.append("div")
                    .attr("class", "chatbadge")
                    .style("background-image", "url(" + twbadges[b].image + ")");
            }
        }
    }
    line.append("span")
        .attr("class", "chatnick")
        .style("color", cm.color)
        .text(cm.user);
    line.append("span")
        .attr("class", "chatcolon")
        .text(":");
    line.append("span")
        .attr("class", "chatmsg")
        .html(replaceTwitchEmotes(cm.msg, cm.emotes));
}

function chatLoop(chat: ChatState) {
    let formatTime = timeFormat("%H:%M");
    let maxMessages = 300;

    if (chat.playerApi.ready) {

        let d = chat.playerApi.getTime();
        let ts = dateToTimestamp(d);

        if (!chat.previousTs) {
            // first load
            loadChat(chat, ts, true);
        } else if (chat.previousTs - 10 > ts || ts > chat.previousTs + 60) {
            // time skip
            chatLoop(clearChat(chat));
            return;
        }

        // check if need to load more messages
        let checkTs = chat.cache.length == 0 ? chat.lastCheckedTs : chat.cache[chat.cache.length-1].ts;
        if ((ts + 10) > checkTs && !chat.cstatus.loading) {
            loadChat(chat, checkTs+1, false);
        }

        while(chat.cache.length != 0) {
            let cm = chat.cache[0];
            if (cm.ts < ts) {
                chat.cache.shift();
                appendMessage(chat.container, cm, chat.badges);
            } else {
                break;
            }
        }

        if (!chat.userScrolling) {
            let messages = chat.container.selectAll("div");
            if (messages.size() > maxMessages) {
                messages.filter(function (d,i) { return i < (messages.size() - maxMessages) }).remove();
            }
        }

        chat.previousTs = ts;
    }

    setTimeout(function() {
        chatLoop(chat);
    }, 500);
}

export function initChat(containerId: string,
                         channel: string,
                         playerApi: PlayerApi,
                         twcid: string,
                         height: number): void {
    let container = select("#" + containerId)
    let chatLog = container.append("div")
        .style("height", height + "px")
        .attr("class", "chatlog");

    let chat: ChatState = {
        container: chatLog,
        containerId: containerId,
        channel: channel,
        playerApi: playerApi,
        cache: [],
        cstatus: { loading: false },
        lastCheckedTs: null,
        previousTs: null,
        userScrolling: false,
        badges: null,
    };

    let chatLogNode = chatLog.node();
    if (chatLogNode instanceof Element) {
        let staydown = new StayDown({
            target: chatLogNode,
            interval: 100,
            callback: function(event: string) {
                switch (event) {
                case 'release':
                    chat.userScrolling = true;
                    break;
                case 'lock':
                    chat.userScrolling = false;
                    break;
                }
            }
        });
    }

    loadBadges(chat, twcid);
    chatLoop(chat);
}
