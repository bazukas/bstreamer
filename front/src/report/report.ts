import { initPlayer } from "./player";
import { initChat } from "./chat";
import { initGraph } from "./graph/graph";
import { GraphsApi, PlayerApi } from "./types";

export function initReport(graphContainer: string, playerContainer: string, chatContainer:string,
                           id: string, channel: string, twcid: string) {
    let height = 700;
    let graphsApi: GraphsApi;
    let playerApi: PlayerApi;

    let playerCallbacks = {
        ready: playerApi ? playerApi.ready : false,
        play: function () {
            if (playerApi && playerApi.play) playerApi.play();
        },
        pause: function () {
            if (playerApi && playerApi.pause) playerApi.pause();
        },
        seek: function (d: Date) {
            if (playerApi && playerApi.seek) playerApi.seek(d);
        },
        getTime: function (): Date {
            if (playerApi && playerApi.getTime) return playerApi.getTime();
        }
    }
    graphsApi = initGraph(graphContainer, id, playerCallbacks, height);

    let graphCallbacks = {
        play: function () {
            if (graphsApi && graphsApi.play != undefined) graphsApi.play();
        },
        pause: function () {
            if (graphsApi && graphsApi.pause != undefined) graphsApi.pause();
        },
    }
    let player = initPlayer(playerContainer, id, graphCallbacks);
    playerApi = player.api;

    initChat(chatContainer, channel, playerApi, twcid, height-player.height+30);
}
