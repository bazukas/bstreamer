import { BaseType, Selection, select } from "d3-selection";
import { utcParse } from "d3-time-format";
import { json } from "d3-request";

import { initSpinner } from "./spin";
import { GraphsApi, PlayerApi } from "./types";

declare namespace Twitch {
    interface Options {
        width: number;
        height: number;
        video: string;
    }

    class Player {
        constructor(containerId: string, options: Options);

        static READY: string;
        static PLAY: string;
        static PAUSE: string;
        static ENDED: string;

        addEventListener(eventid: string, callback: () => void): void;
        removeEventListener(eventid: string, callback: () => void): void;

        setVideo(videoid: string): void;
        play(): void;
        pause(): void;
        seek(timestamp: number): void;
        getCurrentTime(): number;
    }
}

interface VOD {
    start: Date;
    end: Date;
    url: string;
}

function findVod(vods: Array<VOD>, d: Date): VOD | null {
    for (let vod of vods) {
        if (vod.start <= d && vod.end >= d) {
            return vod;
        }
    }
    return null;
}

function nextVod(vods: Array<VOD>, vod: VOD): VOD | null {
    for (let i = 0; i < vods.length - 1; ++i) {
        if (vods[i] == vod)
            return vods[i+1];
    }
    return null;
}

function getVodId(vod: VOD): string {
    return "v" + vod.url.split("/v/")[1];
}

function createPlayer(vods: Array<VOD>, containerId: string, graphsApi: GraphsApi, apifuncs: PlayerApi, width: number, height: number) {
    let currentVod = vods[0];
    let options = {
        width: width,
        height: height,
        video: getVodId(currentVod)
    };
    let player = new Twitch.Player(containerId, options);

    player.addEventListener(Twitch.Player.READY, function() { apifuncs.ready = true });
    player.addEventListener(Twitch.Player.PLAY, graphsApi.play);
    player.addEventListener(Twitch.Player.PAUSE, graphsApi.pause);
    player.addEventListener(Twitch.Player.ENDED, function() {
        let newVod = nextVod(vods, currentVod);
        if (newVod != undefined) changeVod(newVod);
    });

    function changeVod(newVod: VOD, cb?: ()=>void) {
        player.setVideo(getVodId(newVod));
        currentVod = newVod;
        apifuncs.ready = false;
        let readyCb = function () {
            if (cb != undefined) cb();
            player.removeEventListener(Twitch.Player.PLAY, readyCb);
            apifuncs.ready = true;
        }
        player.addEventListener(Twitch.Player.PLAY, readyCb);
    }

    apifuncs.play = function(): void {
        if (apifuncs.ready) {
            player.play();
        }
    }
    apifuncs.pause = function(): void {
        if (apifuncs.ready) {
            player.pause();
        }
    }
    apifuncs.seek = function(d: Date): void {
        if (d < currentVod.start || d > currentVod.end) {
            let newVod = findVod(vods, d);
            if (newVod != undefined) {
                changeVod(newVod, function () {
                    let ts = (d.valueOf() - currentVod.start.valueOf()) / 1000.0;
                    player.seek(ts);
                });
            }
        } else {
            let ts = (d.valueOf() - currentVod.start.valueOf()) / 1000.0;
            if (apifuncs.ready) {
                player.seek(ts);
            }
        }
    }
    apifuncs.getTime = function () {
        if (apifuncs.ready) {
            let ts = player.getCurrentTime()
            return new Date(currentVod.start.getTime() + ts * 1000);
        }
    }
}

function noData(container: Selection<BaseType, {}, HTMLElement, any>) {
    container.html('<p class="center">No VODs available</p>');
}

export function initPlayer(containerId: string, id: string, graphsApi: GraphsApi): {api: PlayerApi, height: number} {
    let api: PlayerApi;
    let height: number;

    let container = select("#" + containerId);
    let containerNode = container.node();

    api = {
        ready: false,
        play: undefined,
        pause: undefined,
        seek: undefined,
        getTime: undefined,
    };

    if (containerNode instanceof Element) {
        let width = containerNode.getBoundingClientRect().width;
        height = width * 9 / 16;
        container.style("height", height + "px");

        let parseTime = utcParse("%Y-%m-%dT%H:%M:%SZ");

        let opts = {
            speed: 2,
            scale: 0.8
        }
        let spinner = initSpinner(containerId);
        json("/dashboard/broadcasts/" + id + "/vods", function(error: Error, data: Array<any>) {
            if (error) throw error;
            spinner.stop();

            if (data.length == 0) {
                noData(container);
            } else {
                data = data.map(function(o) { o.start = parseTime(o.start); o.end = parseTime(o.end); return o });
                data = data.sort(function(a,b) { return a.start - b.start });
                createPlayer(data, containerId, graphsApi, api, width, height);
            }
        });
    }

    return {api: api, height: height};
}
