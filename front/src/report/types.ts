export interface PlayerApi {
    ready: boolean;
    play(): void;
    pause(): void;
    seek(d: Date): void;
    getTime(): Date;
}

export interface GraphsApi {
    play(): void;
    pause(): void;
}
