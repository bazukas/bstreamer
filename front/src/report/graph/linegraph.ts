import { bisector } from "d3-array";
import { easeLinear } from "d3-ease";
import { ScaleTime } from "d3-scale";
import { mouse } from "d3-selection";
import { timeFormat } from "d3-time-format";

import { BSelection } from "./types";
import { GraphData } from "./data";
import { SubGraph, createYScale, createLine, drawLine, drawSubGraph } from "./common";
import { Tooltip, createTooltip } from "./tooltip";

export function drawLineGraph(xscale: ScaleTime<number, number>,
                              data: Array<GraphData>,
                              chart: BSelection,
                              label: string,
                              accessor: (t: GraphData) => number,
                              color: string,
                              width: number,
                              height: number,
                              n: number,
                              drawTicks: boolean): SubGraph {
    let y = createYScale(height, data, accessor);
    let line = createLine(xscale, y, accessor);
    let bisectDate = bisector(function(d: GraphData) { return d.timestamp; }).left;
    let formatTime = timeFormat("%I:%M %p");
    let path: BSelection;
    let tooltip: Tooltip;

    function mousemove(dx: ScaleTime<number, number>): () => void {
        return function() {
            let x0 = dx.invert(mouse(this)[0]),
                i = bisectDate(data, x0, 1),
                d0 = data[i - 1],
                d1 = data[i];
            if (d0 == undefined || d1 == undefined) {
                return;
            }
            let d = x0.valueOf() - d0.timestamp.valueOf() > d1.timestamp.valueOf() - x0.valueOf() ? d1 : d0;
            tooltip.setInfo(label, accessor(d).toString(), formatTime(x0));
            let w = tooltip.getWidth();
            let dxp = dx(d.timestamp),
                xp = dxp,
                dyp = y(accessor(d));
            let yp = dyp - 50;
            if (xp < w/2) {
                xp = w/2;
            } else if (xp > width - w/2) {
                xp = width - w/2;
            }
            if (yp <= 0) {
                yp = dyp + 10;
            }
            tooltip.setDotXY(dxp, dyp);
            tooltip.setXY(xp, yp);
        }
    }

    return drawSubGraph({
        xscale: xscale,
        yscale: y,
        chart: chart,
        label: label,
        width: width,
        height: height,
        n: n,
        drawTicks: drawTicks,
        draw: function(vis: BSelection): void {
            path = drawLine(vis, data, line, color);
        },
        zoomedRedraw: function(vis: BSelection, dx: ScaleTime<number, number>, overlay: BSelection): void {
            path.remove();
            path = drawLine(vis, data, line.x(function(d) { return dx(d.timestamp); }), color);
            overlay.on("mousemove", mousemove(dx));
        },
        animate: function() {
            let pathNode = path.node();
            if (!(pathNode instanceof SVGPathElement))
                return;
            let totalLength = pathNode.getTotalLength();
            path
                .attr("stroke-dasharray", totalLength + " " + totalLength)
                .attr("stroke-dashoffset", totalLength)
                .transition()
                .duration(2000)
                .ease(easeLinear)
                .attr("stroke-dashoffset", 0);
        },
        tooltip: function(vis: BSelection, overlay: BSelection) {
            tooltip = createTooltip(vis, color);
            overlay
                .on("mouseover", function() { tooltip.setOpacity(0.9) })
                .on("mouseout", function() { tooltip.setOpacity(0) })
                .on("mousemove", mousemove(xscale));
        },
        zoomStart: function() {
            tooltip.disable();
        },
        zoomEnd: function() {
            tooltip.enable();
        },
    });
}
