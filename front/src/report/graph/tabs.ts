import { BSelection, ChartBox} from "./types"

type TabButs = {
    but1: BSelection;
    but2: BSelection
}
type Tabs = {
    tab1: BSelection;
    tab2: BSelection
}

export function initTabButtons(container: BSelection): TabButs {
    let tabgroup = container.append("div")
        .attr("class", "clearfix");
    let tab1but = tabgroup.append("a")
        .attr("class", "btn btn-primary bg-blue regular")
        .style("padding-top", "0.25em")
        .style("padding-bottom", "0.25em")
        .text("Main");
    let tab2but = tabgroup.append("a")
        .attr("class", "btn btn-primary bg-gray regular ml1")
        .style("padding-top", "0.25em")
        .style("padding-bottom", "0.25em")
        .text("Chat");
    return {
        but1: tab1but,
        but2: tab2but,
    };
}

export function initTabs(chart: BSelection, box: ChartBox, buts: TabButs): Tabs {
    let tab1 = chart.append("g")
        .attr("clip-path", "url(#tabclip)")
        .append("g")
        .attr("transform", "translate("+[0,0]+")");
    let tab2 = chart.append("g")
        .attr("clip-path", "url(#tabclip)")
        .append("g")
        .attr("transform", "translate("+[0,box.height+box.margin.bottom]+")");
    buts.but1.on("click", function() {
        tab1.transition().duration(500).attr("transform", "translate("+[0,0]+")");
        tab2.transition().duration(500).attr("transform", "translate("+[0,box.height+box.margin.bottom]+")");
        buts.but1.attr("class", "btn btn-primary bg-blue regular");
        buts.but2.attr("class", "btn btn-primary bg-gray regular ml1");
    });
    buts.but2.on("click", function() {
        tab1.transition().duration(500).attr("transform", "translate("+[0,-box.height-2*box.margin.bottom]+")");
        tab2.transition().duration(500).attr("transform", "translate("+[0,0]+")");
        buts.but1.attr("class", "btn btn-primary bg-gray regular");
        buts.but2.attr("class", "btn btn-primary bg-blue regular ml1");
    });

    return {
        tab1: tab1,
        tab2: tab2,
    }
}
