import { axisBottom, axisLeft } from "d3-axis";
import { extent } from "d3-array";
import { BrushBehavior, brushX } from "d3-brush";
import { ScaleTime, scaleTime } from "d3-scale";
import { timeFormat } from "d3-time-format";

import { BSelection } from "./types";
import { GraphData } from "./data";
import { createYScale, createLine, drawLine } from "./common";

export interface Minimap {
    brush: BrushBehavior<{}>;
    brushContainer: BSelection;
    bx: ScaleTime<number, number>;
    setDate: (d: Date) => void;
}

export function drawMinimap(container: BSelection,
                            data: Array<GraphData>,
                            brushCallback: (x: ScaleTime<number, number>) => void,
                            width: number,
                            height: number): Minimap {
    let subHeight = height / 3;
    let minimap = container;

    let timeExtent = extent(data.map(function(o: GraphData): Date { return o.timestamp; }));

    let x = scaleTime()
        .range([0, width])
        .domain(timeExtent);
    let cAccessor = function(d: GraphData): number { return d.chatActivity; };
    let vAccessor = function(d: GraphData): number { return d.viewers; };
    let cy = createYScale(subHeight, data, cAccessor);
    let vy = createYScale(subHeight, data, vAccessor);

    let brush = brushX()
        .extent([[0, 0], [width, height]])
        .on("brush end", function() { brushCallback(x) });

    let cline = createLine(x, cy, cAccessor);
    let vline = createLine(x, vy, vAccessor);

    let xAxis = axisBottom(x)
        .tickFormat(timeFormat("%I %p"));
    let yAxis = axisLeft(createYScale(height, data, vAccessor))
        .tickFormat((d)=>"")
        .tickSize(0);

    function getVis(n: number): BSelection {
        return minimap.append("g")
            .attr("transform", "translate(0," + (n-1)*subHeight + ")");
    }
    drawLine(getVis(1), data, vline, "steelblue");
    drawLine(getVis(2), data, cline, "red");
    minimap.append("g")
        .attr("class", "x axis")
        .attr("transform", "translate(0," + height + ")")
        .call(xAxis);
    minimap.append("g")
        .attr("class", "y axis")
        .call(yAxis);
    let brushc = minimap.append("g")
        .attr("class", "brush")
        .call(brush);

    let dateLine = minimap.append("line")
        .attr("x1", 0)
        .attr("y1", 0)
        .attr("x2", 0)
        .attr("y2", height)
        .attr("class", "dateline");
    function setDate(d: Date) {
        dateLine.attr("x1", x(d))
            .attr("x2", x(d));
    }

    return {
        brush: brush,
        brushContainer: brushc,
        bx: x,
        setDate: setDate,
    };
}

