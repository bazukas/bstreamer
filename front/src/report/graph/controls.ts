import { event, mouse, select } from "d3-selection";

import { BSelection, ChartBox } from "./types";

interface ControlsApi {
    play: () => void;
    pause: () => void;
    seek: (d: Date) => void;
}

interface ControlsCallbacks {
    moveCb: (d: Date) => void;
    play: () => void;
    pause: () => void;
    seek: (d: Date) => void;
}

export function initControls(container: BSelection,
                             box: ChartBox,
                             timeExtent: Array<Date>,
                             callbacks: ControlsCallbacks): ControlsApi {
    let paused = true;

    let controlDiv = container.append("div")
        .attr("class", "clearfix pcontrol")
        .style("width", box.width + "px")
        .style("margin-left", box.margin.left + "px");
    let btn = controlDiv.append("div")
        .attr("class", "pbutton play");
    let btnNode = btn.node();
    if (!(btnNode instanceof Element))
        return;
    let timelinewidth = box.width - btnNode.getBoundingClientRect().width - 4;
    let timeline = controlDiv.append("div")
        .attr("class", "timeline")
        .style("width", timelinewidth + "px");
    let playhead = timeline.append("div")
        .attr("class", "playhead");
    let playheadNode = playhead.node();
    if (!(playheadNode instanceof Element))
        return;
    let playheadwidth = playheadNode.getBoundingClientRect().width;

    btn.on("click", toggle);
    timeline.on("mousedown", timelinemd);
    function toggle() {
        paused = !paused;
        btn.attr("class", "pbutton " + (paused ? "play" : "pause"));
        if (paused) {
            callbacks.pause();
        } else {
            callbacks.play();
        }
    }
    function mouseToPos(pos: number): number {
        return pos - playheadwidth / 2;
    }
    let isMd = false;
    function timelinemd() {
        isMd = true;
        let timelineNode = timeline.node();
        if (!(timelineNode instanceof HTMLElement))
            return;
        moveplayhead(mouseToPos(mouse(timelineNode)[0]), false);
        let w = select(window)
            .on("mousemove", mousemove)
            .on("mouseup", mouseup);
        event.preventDefault();
        function mousemove() {
            if (!(timelineNode instanceof HTMLElement))
                return;
            moveplayhead(mouseToPos(mouse(timelineNode)[0]), false);
        }
        function mouseup() {
            if (!(timelineNode instanceof HTMLElement))
                return;
            isMd = false;
            moveplayhead(mouseToPos(mouse(timelineNode)[0]), true);
            w.on("mousemove", null).on("mouseup", null);
        }
    }

    let posMin = 0,
        posMax = timelinewidth - playheadwidth;
    function moveplayhead(pos: number, up: boolean) {
        pos = Math.max(pos, posMin);
        pos = Math.min(pos, posMax);
        playhead.style("margin-left", pos + "px");
        callbacks.moveCb(posToTime(pos));
        if (up) {
            callbacks.seek(posToTime(pos));
        }
    }

    let timeDiff = timeExtent[1].getTime() - timeExtent[0].getTime();
    function posToTime(pos: number): Date {
        return new Date(timeExtent[0].getTime() + timeDiff * (pos / posMax));
    }
    function timeToPos(d: Date): number {
        if (!d)
            return 0;
        return (d.getTime() - timeExtent[0].getTime()) / timeDiff * posMax;
    }

    function play(): void {
        paused = false;
        btn.attr("class", "pbutton pause");
    }
    function pause(): void {
        paused = true;
        btn.attr("class", "pbutton play");
    }
    function seek(d: Date): void {
        if (!isMd) {
            moveplayhead(timeToPos(d), false);
        }
    }

    return {
        play: play,
        pause: pause,
        seek: seek,
    }
}

