import { extent } from "d3-array";
import { event, select } from "d3-selection";
import { json } from "d3-request";
import { ScaleTime, scaleTime } from "d3-scale";
import { interval } from "d3-timer";
import { zoom, zoomIdentity } from "d3-zoom";

import { initSpinner } from "../spin";
import { GraphsApi, PlayerApi } from "./../types";
import { BSelection, ChartBox} from "./types";
import { RawData, BroadcastData, GraphData, parseBroadcastData } from "./data";
import { initTabButtons, initTabs } from "./tabs";
import { drawMinimap } from "./minimap";
import { initControls } from "./controls";
import { drawEventGraph } from "./eventgraph";
import { drawLineGraph } from "./linegraph";
import { drawWordClouds } from "./wordcloud";
import { drawReactions } from "./reactions";
import { SubGraph } from "./common";

function createClipPaths(chart: BSelection, box: ChartBox): void {
  chart.append("clipPath")
      .attr("id", "clip")
    .append("rect")
      .attr("x", 0)
      .attr("y", 0)
      .attr("width", box.width)
      .attr("height", box.height);

  chart.append("clipPath")
      .attr("id", "tabclip")
    .append("rect")
      .attr("x", -box.margin.left)
      .attr("y", -box.margin.top)
      .attr("width", box.width + box.margin.left + box.margin.right)
      .attr("height", box.height + box.margin.top + box.margin.bottom);
}

export function initGraph(containerId: string, id: string, playerApi: PlayerApi, fullHeight: number): GraphsApi {
    let api: GraphsApi = {
        play: null,
        pause: null,
    };
    let container = select("#" + containerId);
    let containerNode = container.node();

    if (!(containerNode instanceof Element)) {
        return;
    }

    let margin = {top: 20, right: 0, bottom: 30, left: 40};
    let minimapHeight = 40;
    let box: ChartBox = {
        width: containerNode.getBoundingClientRect().width - margin.left - margin.right,
        height: fullHeight - margin.top - 2 * margin.bottom - minimapHeight,
        margin: margin
    };

    let spinner = initSpinner(containerId);

    let x = scaleTime()
        .range([0, box.width]);

    let tabButs = initTabButtons(container);

    let svg = container.append("svg")
        .attr("width", box.width + box.margin.left + box.margin.right)
        .attr("height", fullHeight)
    let chart = svg.append("g")
        .attr("transform", "translate(" + box.margin.left + "," + box.margin.top + ")");
    createClipPaths(chart, box);

    let tabs = initTabs(chart, box, tabButs);

    json("/dashboard/broadcasts/" + id + "/data", function(error: Error, data: RawData) {
        if (error) throw error;

        let broadcastData = parseBroadcastData(data);

        spinner.stop();

        let timeExtent = extent(broadcastData.graphData.map(function(d: GraphData): Date {
            return d.timestamp;
        }));
        x.domain(timeExtent);

        let subgraphs: Array<SubGraph> = [];

        // minimap
        function brushed(bx: ScaleTime<number, number>) {
            if (event.sourceEvent && event.sourceEvent.type === "zoom") return; // ignore brush-by-zoom
            let s = event.selection || bx.range();
            x.domain(s.map(bx.invert, bx));
            subgraphs.forEach(function(s: SubGraph): void {
                s.zoomCallback(x);
            });
            chart.call(zoom().transform, zoomIdentity.scale(box.width / (s[1] - s[0])).translate(-s[0], 0));
        }
        let minimapContainer = chart.append("g")
            .attr("transform", "translate(0," + (box.height + box.margin.bottom) + ")");
        let minimap = drawMinimap(minimapContainer, broadcastData.graphData, brushed, box.width, minimapHeight);

        // zoom
        let maxScale = Math.max((timeExtent[1].valueOf() - timeExtent[0].valueOf()) / (300000 * 14), 1);
        let mzoom = zoom()
            .scaleExtent([1, maxScale])
            .translateExtent([[0,box.height], [box.width + margin.left, 0]])
            .on("start", function() {
                subgraphs.forEach(function(s: SubGraph): void {
                    s.zoomStart();
                });
            })
            .on("end", function() {
                subgraphs.forEach(function(s: SubGraph): void {
                    s.zoomEnd();
                });
            })
            .on("zoom", function() {
                if (event.sourceEvent && event.sourceEvent.type === "brush") return; // ignore zoom-by-brush
                let t = event.transform;
                // hack
                t.x = Math.max(t.x, (-box.width) * t.k + box.width);
                x.domain(t.rescaleX(minimap.bx).domain());
                subgraphs.forEach(function(s: SubGraph): void {
                    s.zoomCallback(x);
                });
                minimap.brushContainer.call(minimap.brush.move, x.range().map(t.invertX, t));
            });
        chart.call(mzoom)
            .on("dblclick.zoom", null);
        chart.call(mzoom.transform, zoomIdentity.scale(maxScale).translate(0, 0));


        // graphs
        subgraphs.push(drawLineGraph(x, broadcastData.graphData, tabs.tab1,
                          "Viewers", (g: GraphData) => g.viewers,
                          "steelblue", box.width, box.height/3, 1, false));
        subgraphs.push(drawLineGraph(x, broadcastData.graphData, tabs.tab1,
                          "Chat Activity", (g: GraphData) => g.chatActivity,
                          "red", box.width, box.height/3, 2, false));
        subgraphs.push(drawEventGraph(x, broadcastData.eventData, tabs.tab1, "Events",
                           box.width, box.height/3, 3, true));

        subgraphs.push(drawLineGraph(x, broadcastData.graphData, tabs.tab2,
                          "Lurker Activity", (g: GraphData) => g.lurkerActivity,
                          "chocolate", box.width, box.height/3, 1, false));
        subgraphs.push(drawWordClouds(x, broadcastData.wordClouds, tabs.tab2, "Word Cloud",
                           box.width, box.height/3, 2, false));
        subgraphs.push(drawReactions(x, broadcastData.reactions, playerApi, tabs.tab2, "Reactions",
                           box.width, box.height/3, 3, true));

        subgraphs.forEach(function(s: SubGraph): void {
            s.animate();
        });

        // controls
        let controls = initControls(container, box, timeExtent, {
            moveCb: function(ts: Date) {
                subgraphs.forEach(function(s: SubGraph): void {
                    s.setDate(ts);
                });
                minimap.setDate(ts);
            },
            play: playerApi.play,
            pause: playerApi.pause,
            seek: playerApi.seek,
        });
        api.play = controls.play;
        api.pause = controls.pause;

        let timer = interval(function () {
            controls.seek(playerApi.getTime());
        }, 1000);
    });

    return api;
}
