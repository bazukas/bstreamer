import { ScaleTime, scaleLinear } from "d3-scale";
import { timeFormat } from "d3-time-format";

import { BSelection } from "./types";
import { EventData, isGameChangeEvent, isStatusChangeEvent, isSlowEvent } from "./data";
import { SubGraph, drawSubGraph } from "./common";
import { Tooltip, createTooltip } from "./tooltip";

function eventLabel(bevent: EventData): string {
    switch (bevent.eventid) {
    case "subs_on": return "Subscriber Only";
    case "subs_off": return "Subscriber Only";
    case "slow_on": return "Slow Mode";
    case "slow_off": return "Slow Mode";
    case "emote_only_on": return "Emote Only";
    case "emote_only_off": return "Emote Only";
    case "r9k_on": return "R9K Mode";
    case "r9k_off": return "R9K Mode";
    case "game_change": return "Game";
    case "status_change": return "Title";
    }
}

function eventData(bevent: EventData): string {
    switch (bevent.eventid) {
    case "game_change":
        if (isGameChangeEvent(bevent.event)) {
            return bevent.event.game;
        }
    case "status_change":
        if (isStatusChangeEvent(bevent.event)) {
            return bevent.event.status;
        }
    case "slow_on":
        if (isSlowEvent(bevent.event)) {
            return bevent.event.seconds + " seconds";
        }
    case "slow_off": return "Off"
    case "subs_on": return "On"
    case "subs_off": return "Off"
    case "emote_only_on": return "On"
    case "emote_only_off": return "Off"
    case "r9k_on": return "On"
    case "r9k_off": return "Off"
    }
}

function eventIcon(bevent: EventData): string {
    switch (bevent.eventid) {
    case "subs_on": return "/static/images/money-bag.svg";
    case "subs_off": return "/static/images/money-bag.svg";
    case "slow_on": return "/static/images/snail.svg";
    case "slow_off": return "/static/images/snail.svg";
    case "emote_only_on": return "/static/images/smile.svg";
    case "emote_only_off": return "/static/images/smile.svg";
    case "r9k_on": return "/static/images/r9k.svg";
    case "r9k_off": return "/static/images/r9k.svg";
    case "game_change": return "/static/images/gamepad.svg";
    case "status_change": return "/static/images/tag.svg";
    }
}

interface EventBox {
    vis: BSelection;
    bevent: EventData;
    getX: () => number;
    getY: () => number;
    setX: (x: number) => void;
    getWidth: () => number;
    getRightX: () => number;
    setWidth: (width?: number) => void;
}

function drawEvent(vis: BSelection,
                   bevent: EventData,
                   xscale: ScaleTime<number, number>,
                   totalHeight: number): EventBox {
    let padding = 0.5;
    let width = totalHeight / 6 - padding * 2;
    let height = width;

    let x = xscale(bevent.timestamp);
    let y: number;
    let n: number;
    if (bevent.eventid == "game_change") {
        n = 0;
    } else if (bevent.eventid == "status_change") {
        n = 1;
    } else if (bevent.eventid == "slow_on" || bevent.eventid == "slow_off") {
        n = 2;
    } else if (bevent.eventid == "subs_on" || bevent.eventid == "subs_off") {
        n = 3;
    } else if (bevent.eventid == "emote_only_on" || bevent.eventid == "emote_only_off") {
        n = 4;
    } else if (bevent.eventid == "r9k_on" || bevent.eventid == "r9k_off") {
        n = 5;
    }
    y = (height + 2 * padding) * n + padding;


    let clipg = vis.append("g")
        .attr("clip-path", "url(#clip)");
    let ev = clipg.append("g")
        .attr("transform", "translate(" + x + "," + y + ")");
    let rect = ev.append("rect")
        .attr("class", bevent.eventid)
        .attr("rx", 3)
        .attr("ry", 3)
        .attr("width", width)
        .attr("height", height);
    ev.append("image")
        .attr("width", width)
        .attr("height", height)
        .attr("xlink:href", eventIcon(bevent));

    let text = ev.append("text")
        .style("cursor", "default")
        .attr("class", "eventText")
        .attr("x", width+2)
        .attr("y", height/2)
        .attr("alignment-baseline", "middle")
        .text(eventData(bevent));

    let textNode = text.node();
    if (!(textNode instanceof SVGTextElement))
        return;
    let rectwidth = width + textNode.getBBox().width + 8;
    rect.attr("width", rectwidth);

    function setTextWidth(twidth: number) {
        if (!(textNode instanceof SVGTextElement))
            return;
        let ctext = text.text(),
            textLength = textNode.getComputedTextLength();
        while (textLength > twidth && ctext.length > 0) {
            ctext = ctext.slice(0, -1);
            if (ctext.length == 0) {
                text.text("");
                return;
            }
            text.text(ctext + '...');
            textLength = textNode.getComputedTextLength();
        }
    }

    let evNode = ev.node();
    return {
        vis: ev,
        bevent: bevent,
        getX: function() { return x; },
        getY: function() { return y; },
        setX: function(nx: number) {
            x = nx;
            ev.attr("transform", "translate(" + x + "," + y + ")");
        },
        getWidth: function() {
            if (evNode instanceof Element)
                return evNode.getBoundingClientRect().width;
        },
        getRightX: function() {
            if (evNode instanceof Element)
                return evNode.getBoundingClientRect().width + x
        },
        setWidth: function(nwidth?: number) {
            if (["game_change", "status_change"].indexOf(bevent.eventid) == -1)
                return
            if (nwidth && nwidth >= width) {
                rect.attr("width", nwidth);
                setTextWidth(nwidth - width - 2);
            } else if (nwidth) {
                rect.attr("width", width);
                text.text("");
            } else {
                rect.attr("width", rectwidth);
                text.text(eventData(bevent));
            }
        },
    };
}

function positionEvents(events: Array<EventBox>, dx: ScaleTime<number, number>) {
    let e: EventBox;
    let le: EventBox;
    let maxNegX: number;
    let maxNegEv: EventBox;
    let minPosX: number;
    for (let i=0; i < events.length; ++i) {
        e = events[i];
        if (i > 0)
            le = events[i-1];
        let nx = dx(e.bevent.timestamp);
        e.setWidth();
        if (i > 0 && le.getRightX() + 1 >= nx) {
            le.setWidth(nx - 1 - le.getX());
            if (le.getRightX() + 1 >= nx) {
                nx = le.getRightX() + 1;
            }
        }
        e.setX(nx);
        if (nx < 0 && (!maxNegX || nx > maxNegX)) {
            maxNegX = nx;
            maxNegEv = e;
        } else if (nx >= 0 && (!minPosX || nx < minPosX)) {
            minPosX = nx;
        }
    }

    if (maxNegEv) {
        let w = maxNegEv.getWidth();
        if (minPosX) {
            maxNegEv.setX(Math.min(minPosX-1,w) - w);
        } else {
            maxNegEv.setX(1);
        }
    }
}

export function drawEventGraph(xscale: ScaleTime<number, number>,
                               data: Array<EventData>,
                               chart: BSelection,
                               label: string,
                               width: number,
                               height: number,
                               n: number,
                               drawTicks: boolean): SubGraph {
    let y = scaleLinear()
        .range([height, 0]);
    let allevents: Array<EventBox> = [];
    let events: Array<Array<EventBox>> = [];
    let tooltip: Tooltip;
    let formatTime = timeFormat("%I:%M %p");

    function mouseover(dx: ScaleTime<number, number>, r: EventBox): () => void {
        return function() {
            tooltip.setOpacity(0.9);
            tooltip.setInfo(eventLabel(r.bevent), eventData(r.bevent), formatTime(r.bevent.timestamp));
            let y = r.getY();
            let x = dx(r.bevent.timestamp) + r.getWidth()/2;
            let w = tooltip.getWidth();
            let h = tooltip.getHeight();
            x = x + 5;
            if (x < w/2) {
                x = w/2;
            } else if (x > width - w/2) {
                x = width - w/2;
            }
            tooltip.setXY(x,y-h);
        }
    }

    return drawSubGraph({
        xscale: xscale,
        yscale: y,
        chart: chart,
        label: label,
        width: width,
        height: height,
        n: n,
        drawTicks: drawTicks,
        draw: function(vis: BSelection): void {
            allevents = data.map(function(o) { return drawEvent(vis, o, xscale, height); });
            events = [
                allevents.filter(function(r) { return r.bevent.eventid == "game_change" }),
                allevents.filter(function(r) { return r.bevent.eventid == "status_change" }),
                allevents.filter(function(r) { return ["slow_on", "slow_off"].indexOf(r.bevent.eventid) != -1 }),
                allevents.filter(function(r) { return ["subs_on", "subs_off"].indexOf(r.bevent.eventid) != -1 }),
                allevents.filter(function(r) { return ["emote_only_on", "emote_only_off"].indexOf(r.bevent.eventid) != -1 }),
                allevents.filter(function(r) { return ["r9k_on", "r9k_off"].indexOf(r.bevent.eventid) != -1 }),
            ];
            events.forEach(function(e) {
                positionEvents(e, xscale);
            });
        },
        zoomedRedraw: function(vis: BSelection, dx: ScaleTime<number, number>, overlay: BSelection): void {
            events.forEach(function(e) {
                positionEvents(e, dx);
            });
            allevents.forEach(function(r) {
                r.vis.on("mouseover", mouseover(dx,r));
            });
        },
        animate: function() {
        },
        tooltip: function(vis: BSelection, overlay: BSelection) {
            tooltip = createTooltip(vis, "black");
            tooltip.setDotXY(-100, -100);
            allevents.map(function(r) {
                r.vis
                    .on("mouseout", function() { tooltip.setOpacity(0) })
                    .on("mouseover", mouseover(xscale, r));
            });
        },
        zoomStart: function() {
        },
        zoomEnd: function() {
        },
    });
}
