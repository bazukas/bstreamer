import { extent } from "d3-array";
import { ScaleTime, ScaleLinear, ScaleOrdinal, scaleLinear, scaleOrdinal, schemeCategory20 } from "d3-scale";
import d3cloud = require("d3.layout.cloud");

import { WordCloud, WordValue } from "./data";
import { BSelection } from "./types";
import { SubGraph, drawSubGraph } from "./common";

interface WordBox {
    move: (dx: ScaleTime<number, number>) => void;
}

function drawCloud(vis: BSelection,
                   xscale: ScaleTime<number, number>,
                   wc: WordCloud,
                   height: number,
                   wordWeightScale: ScaleLinear<number, number>,
                   wordFill: ScaleOrdinal<string, string>): WordBox {
    let x1 = xscale(wc.start),
        x2 = xscale(wc.end);
    let clipg = vis.append("g")
        .attr("clip-path", "url(#clip)");
    let cloud = clipg.append("g")
        .attr("transform", "translate(" + x1 + ",0)");
    let padding = 10,
        cloudwidth = x2-x1,
        cloudheight = height-padding;
    let wordSizeScale = scaleLinear()
        .range([10, 20])
        .domain(extent(wc.words, function(w) { return w.value }));

    d3cloud()
        .size([cloudwidth, cloudheight])
        .words(wc.words)
        .rotate(0)
        .padding(3)
        .fontSize(function(d: WordValue) { return wordSizeScale(d.value); })
        .on("end", function(words) {
            cloud.selectAll("text").data(words)
                .enter()
                .append("text")
                .style("cursor", "default")
                .style("fill", function(d,i) { return wordFill(i.toString()) })
                .text(function(d) { return d.text; })
                .attr("text-rendering", "geometricPrecision")
                .attr("font-size", function(d) { return d.size; })
                .attr("font-weight", function(d) { return wordWeightScale(d.size); })
                .attr("text-anchor", "middle")
                .attr("x", function(d) { return d.x + cloudwidth/2; })
                .attr("y", function(d) { return d.y + cloudheight/2+padding/2; });
        }).start();

    return {
        move: function(dx: ScaleTime<number, number>): void {
            let x1 = dx(wc.start),
                x2 = dx(wc.end);
            let scale = 1;
            if (cloudwidth) {
                scale = (x2-x1) / cloudwidth;
            }
            cloud.attr("transform", "translate(" + x1 + ",0)scale(" + scale + ",1)");
        }
    };
}

export function drawWordClouds(xscale: ScaleTime<number, number>,
                               data: Array<WordCloud>,
                               chart: BSelection,
                               label: string,
                               width: number,
                               height: number,
                               n: number,
                               drawTicks: boolean): SubGraph {
    let y = scaleLinear()
        .range([height, 0]);
    let sizeExtent = extent([].concat.apply([], data.map(function(o) { return o.words })), function(d: WordValue) {
        return d.value;
    });
    let wordWeightScale = scaleLinear()
        .range([500, 800])
        .domain(sizeExtent);
    let wordFill = scaleOrdinal(schemeCategory20);
    let boxes: Array<WordBox> = [];

    return drawSubGraph({
        xscale: xscale,
        yscale: y,
        chart: chart,
        label: label,
        width: width,
        height: height,
        n: n,
        drawTicks: drawTicks,
        draw: function(vis: BSelection): void {
            boxes = data.map(function(wc: WordCloud) {
                return drawCloud(vis, xscale, wc, height, wordWeightScale, wordFill);
            });
        },
        zoomedRedraw: function(vis: BSelection, dx: ScaleTime<number, number>, overlay: BSelection): void {
            boxes.forEach(function(b) { b.move(dx); });
        },
        animate: function() {
        },
        tooltip: function(vis: BSelection, overlay: BSelection) {
        },
        zoomStart: function() {
        },
        zoomEnd: function() {
        },
    });
}
