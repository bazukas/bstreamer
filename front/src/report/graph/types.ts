import { BaseType, Selection } from "d3-selection";

export type BSelection = Selection<BaseType, {}, HTMLElement, any>;

export interface ChartBox {
    width: number;
    height: number;
    margin: {
        top: number;
        bottom: number;
        left: number;
        right: number;
    };
}
