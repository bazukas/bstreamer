import { utcParse } from "d3-time-format";

export interface WordValue {
    text: string;
    value: number;
};

interface SlowEvent {
    event: string;
    seconds: number;
}
interface GameChangeEvent {
    event: string;
    game: string;
}
interface StatusChangeEvent {
    event: string;
    status: string;
}
type Event = string | SlowEvent | GameChangeEvent | StatusChangeEvent;
export function isSlowEvent(object: any): object is SlowEvent {
    return 'seconds' in object;
}
export function isGameChangeEvent(object: any): object is GameChangeEvent {
    return 'game' in object;
}
export function isStatusChangeEvent(object: any): object is StatusChangeEvent {
    return 'status' in object;
}

interface RawGraphData {
    timestamp: string;
    viewers: number;
    followers: number;
    chat_activity: number;
    mod_activity: number;
    lurker_activity: number;
}
interface RawEventData {
    timestamp: string;
    event: Event;
}
interface RawWordCloud {
    start: string;
    end: string;
    word_map: {
        [key: string]: number;
    }
}
interface RawReaction {
    reaction: string;
    timestamp: string;
    size: number;
}

export interface GraphData {
    timestamp: Date;
    viewers: number;
    followers: number;
    chatActivity: number;
    modActivity: number;
    lurkerActivity: number;
}
export interface EventData {
    timestamp: Date;
    event: Event;
    eventid: string;
}
export interface WordCloud {
    start: Date;
    end: Date;
    words: Array<WordValue>;
}
export interface Reaction {
    reaction: string;
    timestamp: Date;
    size: number;
}

export interface RawData {
    graph_data: Array<RawGraphData>;
    event_data: Array<RawEventData>;
    word_cloud: Array<RawWordCloud>;
    reactions: Array<RawReaction>;
}

export interface BroadcastData {
    graphData: Array<GraphData>;
    eventData: Array<EventData>;
    wordClouds: Array<WordCloud>;
    reactions: Array<Reaction>;
}

function parseTime(time: string): Date {
    let parseTime1 = utcParse("%Y-%m-%dT%H:%M:%S.%LZ");
    let parseTime2 = utcParse("%Y-%m-%dT%H:%M:%SZ");
    return parseTime1(time) || parseTime2(time);
}

export function parseBroadcastData (data: RawData): BroadcastData {
    return {
        graphData: data.graph_data.map(function(o: RawGraphData): GraphData {
            return {
                timestamp: parseTime(o.timestamp),
                viewers: o.viewers,
                followers: o.followers,
                chatActivity: o.chat_activity,
                modActivity: o.mod_activity,
                lurkerActivity: o.lurker_activity
            };
        }),
        eventData: data.event_data.map(function(o: RawEventData): EventData {
            let eventid: string;
            if (typeof o.event === "string")
                eventid = o.event;
            else if (isSlowEvent(o.event) || isGameChangeEvent(o.event) || isStatusChangeEvent(o.event))
                eventid = o.event.event;
            return {
                timestamp: parseTime(o.timestamp),
                event: o.event,
                eventid: eventid,
            };
        }),
        wordClouds: data.word_cloud.map(function(o: RawWordCloud): WordCloud {
            return {
                start: parseTime(o.start),
                end: parseTime(o.end),
                words: Object.keys(o.word_map).map(function(k: string): WordValue {
                    return {
                        text: k,
                        value: o.word_map[k],
                    }
                }),
            };
        }),
        reactions: data.reactions.map(function(o: RawReaction): Reaction {
            return {
                reaction: o.reaction,
                timestamp: parseTime(o.timestamp),
                size: o.size,
            };
        }),
    };
}
