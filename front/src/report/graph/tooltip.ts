import { BSelection } from "./types";

export interface Tooltip {
    setInfo: (label: string, data: string, time: string) => void;
    setOpacity: (op: number) => void;
    setXY: (x: number, y: number) => void;
    setDotXY: (x: number, y: number) => void;
    getWidth: () => number;
    getHeight: () => number;
    disable: () => void;
    enable: () => void;
}

export function createTooltip(vis: BSelection, color: string): Tooltip {
    let tooltip = vis.append("g")
        .style("opacity", 0);
    let rect = tooltip.append("rect")
        .attr("rx", 5)
        .attr("ry", 5)
        .attr("class", "tooltipbox");
    let text = tooltip.append("text")
        .attr("class", "tooltip");
    let line1 = text.append("tspan")
        .attr("x", 0)
        .attr("dy", "1.2em")
        .attr("text-anchor", "middle");
    let line2 = text.append("tspan")
        .attr("x", 0)
        .attr("dy", "1.2em")
        .attr("text-anchor", "middle");
    let dot = vis.append("circle")
        .attr("r", 3)
        .style("opacity", 0)
        .style("fill", color);

    let disabled = false;
    let sop = 0;

    function drawRect(): void {
        let padding = 6;
        let textNode = text.node();
        if (!(textNode instanceof SVGTextElement)) {
            return;
        }
        let bbox = textNode.getBBox();
        rect
            .attr("x", bbox.x - padding)
            .attr("y", bbox.y - padding)
            .attr("width", bbox.width + (padding*2))
            .attr("height", bbox.height + (padding*2));
    }

    function setInfo(label: string, data: string, time: string): void {
        line1.text(label + ": " + data);
        line2.text("Time: " + time);
        drawRect();
    }
    function setOpacity(op: number): void {
        sop = op;
        if (!disabled) {
            tooltip.transition().duration(100).style("opacity", op);
            dot.transition().duration(100).style("opacity", op);
        }
    }
    function setXY(x: number, y: number): void {
        tooltip.attr("transform", "translate(" + x + "," + y + ")");
    }
    function setDotXY(x: number, y: number): void {
        dot.attr("cx", x).attr("cy", y);
    }
    function getWidth(): number {
        let tooltipNode = tooltip.node();
        if (!(tooltipNode instanceof Element))
            return;
        return tooltipNode.getBoundingClientRect().width;
    }
    function getHeight(): number {
        let tooltipNode = tooltip.node();
        if (!(tooltipNode instanceof Element))
            return;
        return tooltipNode.getBoundingClientRect().height;
    }
    function disable(): void {
        disabled = true;
        tooltip.transition().duration(100).style("opacity", 0);
        dot.transition().duration(100).style("opacity", 0);
    }
    function enable(): void {
        disabled = false;
        setOpacity(sop);
    }

    return {
        setInfo: setInfo,
        setOpacity: setOpacity,
        setXY: setXY,
        setDotXY: setDotXY,
        getWidth: getWidth,
        getHeight: getHeight,
        disable: disable,
        enable: enable,
    }
}
