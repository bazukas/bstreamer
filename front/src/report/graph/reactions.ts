import { extent } from "d3-array";
import { timeFormat } from "d3-time-format";
import { ScaleTime, ScaleLinear, ScaleOrdinal, scaleLinear, scaleOrdinal, schemeCategory20 } from "d3-scale";

import { PlayerApi } from "../types";
import { BSelection } from "./types";
import { Reaction } from "./data";
import { SubGraph, drawSubGraph } from "./common";
import { Tooltip, createTooltip } from "./tooltip";

interface ReactionBox {
    vis: BSelection;
    reaction: Reaction;
    setY: (y: number) => void;
    getY: () => number;
    setX: (x: number) => void;
    width: number;
    height: number;
}

function drawReaction(reaction: Reaction,
                      i: number,
                      xscale: ScaleTime<number, number>,
                      height: number,
                      vis: BSelection,
                      wordFill: ScaleOrdinal<string, string>,
                      wordWeightScale: ScaleLinear<number, number>,
                      wordSizeScale: ScaleLinear<number, number>,
                      playerApi: PlayerApi): ReactionBox {
    let padding = 5;
    let x = xscale(reaction.timestamp);
    let y = height / 2;

    let clipg = vis.append("g")
    .attr("clip-path", "url(#clip)");
    let ev = clipg.append("g")
    .attr("transform", "translate(" + x + "," + y + ")");
    let rect = ev.append("rect")
    .style("cursor", "pointer")
    .style("fill", wordFill(i.toString()))
    .attr("rx", 3)
    .attr("ry", 3);
    let text = ev.append("text")
    .style("cursor", "pointer")
    .style("fill", "#EEE")
    .style("pointer-events", "none")
    .attr("font-weight", wordWeightScale(reaction.size))
    .attr("font-size", wordSizeScale(reaction.size))
    .attr("alignment-baseline", "middle")
    .text(reaction.reaction);

    let textNode = text.node();
    if (!(textNode instanceof Element))
        return;
    let rectwidth = textNode.getBoundingClientRect().width + 2*padding;
    let rectheight = textNode.getBoundingClientRect().height + 2*padding;
    rect.attr("width", rectwidth);
    rect.attr("height", rectheight);
    text.attr("x", padding)
    text.attr("y", rectheight/2);

    rect.on("click", function() {
        playerApi.seek(new Date(reaction.timestamp.valueOf()-10*1000));
    });

    function setY(ny: number): void {
        y = ny;
        ev.attr("transform", "translate(" + x + "," + y + ")");
    }
    setY(y-rectheight/2);

    return {
        vis: ev,
        reaction: reaction,
        setY: setY,
        getY: function() { return y; },
        setX: function(nx) {
            x = nx;
            ev.attr("transform", "translate(" + x + "," + y + ")");
        },
        width: rectwidth,
        height: rectheight,
    };
}

function positionReactions(reactions: Array<ReactionBox>,
                           dx: ScaleTime<number, number>,
                           height: number) {
    let occupiedPositions: Array<Array<number>> = [];
    function canBePlaced(x1: number, y1: number, x2: number, y2: number): boolean {
        for (let i=0; i < occupiedPositions.length; ++i) {
            let p = occupiedPositions[i];
            let ox1 = occupiedPositions[i][0],
                oy1 = occupiedPositions[i][1],
                ox2 = occupiedPositions[i][2],
                oy2 = occupiedPositions[i][3];
            if (!(x2<ox1 || ox2<x1 || y2<oy1 || oy2<y1)) {
                return false;
            }
        }
        return true;
    }
    reactions.forEach(function(r) {
        let nx = dx(r.reaction.timestamp);
        let y1 = r.getY(),
            y2 = y1+r.height;
        let placed = false;
        let shift = 0;
        let sign = -1;
        let oy1 = y1,
            oy2 = y2;
        while (y1 > 0 && y2 < height && !placed) {
            if (canBePlaced(nx,y1,nx+r.width,y2)) {
                r.setX(nx);
                r.setY(y1);
                occupiedPositions.push([nx,y1,nx+r.width,y2]);
                placed = true;
            } else {
                shift += 5;
                sign *= -1;
                y1 = oy1 + shift * sign;
                y2 = oy2 + shift * sign;
            }
        }
        if (!placed) {
            r.setX(-10000);
        }
    });
}

export function drawReactions(xscale: ScaleTime<number, number>,
                               data: Array<Reaction>,
                               playerApi: PlayerApi,
                               chart: BSelection,
                               label: string,
                               width: number,
                               height: number,
                               n: number,
                               drawTicks: boolean): SubGraph {
    let y = scaleLinear()
        .range([height, 0]);
    let sizeExtent = extent(data, function(d) {
        return d.size;
    });
    let wordWeightScale = scaleLinear()
        .range([500, 800])
        .domain(sizeExtent);
    let wordSizeScale = scaleLinear()
        .range([13, 23])
        .domain(sizeExtent);
    let wordFill = scaleOrdinal(schemeCategory20);
    let reactions: Array<ReactionBox> = [];
    let tooltip: Tooltip;
    let formatTime = timeFormat("%I:%M %p");
    function mouseover(dx: ScaleTime<number, number>, r: ReactionBox): () => void {
        return function() {
            tooltip.setOpacity(0.9);
            tooltip.setInfo("Size", r.reaction.size.toString(), formatTime(r.reaction.timestamp));
            let y = r.getY();
            let x = dx(r.reaction.timestamp) + r.width/2;
            let w = tooltip.getWidth();
            let h = tooltip.getHeight();
            if (x < w/2) {
                x = w/2;
            } else if (x > width - w/2) {
                x = width - w/2;
            }
            tooltip.setXY(x,y-h);
        }
    }

    return drawSubGraph({
        xscale: xscale,
        yscale: y,
        chart: chart,
        label: label,
        width: width,
        height: height,
        n: n,
        drawTicks: drawTicks,
        draw: function(vis: BSelection): void {
            reactions = data.map(function(reaction: Reaction, i: number) {
                return drawReaction(reaction, i, xscale, height, vis,
                                    wordFill, wordWeightScale, wordSizeScale, playerApi);
            });
            positionReactions(reactions, xscale, height);
        },
        zoomedRedraw: function(vis: BSelection, dx: ScaleTime<number, number>, overlay: BSelection): void {
            positionReactions(reactions, dx, height);
            reactions.forEach(function(r) {
                r.vis.on("mouseover", mouseover(dx,r));
            });
        },
        animate: function() {
        },
        tooltip: function(vis: BSelection, overlay: BSelection) {
            tooltip = createTooltip(vis, "black");
            tooltip.setDotXY(-100, -100);
            reactions.forEach(function(r: ReactionBox) {
                r.vis
                    .on("mouseout", function() { tooltip.setOpacity(0) })
                    .on("mouseover", mouseover(xscale, r));
            });
        },
        zoomStart: function() {
        },
        zoomEnd: function() {
        },
    });
}
