import { extent, mean } from "d3-array";
import { axisBottom, axisLeft } from "d3-axis";
import { ScaleTime, ScaleLinear, scaleLinear } from "d3-scale";
import { Line, line, curveMonotoneX } from "d3-shape";

import { BSelection } from "./types"
import { GraphData } from "./data"

function getDomain(data: Array<GraphData>, accessor: (t: GraphData)=>number): [number, number] {
    let m = mean(data, accessor);
    let dextent = extent(data, accessor);
    let min = dextent[0],
        max = Math.max(dextent[1], 2*m - min);
    return [min, max];
}

export function createYScale(height: number, data: Array<GraphData>, accessor: (t: GraphData)=>number): ScaleLinear<number, number> {
    return scaleLinear()
        .range([height, 0])
        .domain(getDomain(data, accessor));
}

export function createLine(x: ScaleTime<number, number>, y: ScaleLinear<number, number>, accessor: (t: GraphData)=>number): Line<GraphData> {
    return line<GraphData>()
        .curve(curveMonotoneX)
        .x(function(d) { return x(d.timestamp); })
        .y(function(d) { return y(accessor(d)); });
}

export function drawLine(vis: BSelection, data: Array<GraphData>, line: Line<GraphData>, color: string): BSelection {
    return vis.append("path")
        .datum(data)
        .style("stroke", color)
        .attr("clip-path", "url(#clip)")
        .attr("class", "line")
        .attr("d", line);
}

interface SubGraphOptions {
    xscale: ScaleTime<number, number>;
    yscale: ScaleLinear<number, number>;
    chart: BSelection;
    label: string;
    width: number;
    height: number;
    n: number;
    drawTicks: boolean;
    draw: (vis: BSelection) => void;
    zoomedRedraw: (vis: BSelection, dx: ScaleTime<number, number>, overlay: BSelection) => void;
    animate: () => void;
    tooltip: (vis: BSelection, overlay: BSelection) => void;
    zoomStart: () => void;
    zoomEnd: () => void;
}

export interface SubGraph {
    zoomStart: () => void;
    zoomEnd: () => void;
    zoomCallback: (dx: ScaleTime<number, number>) => void;
    animate: () => void;
    setDate: (d?: Date) => void;
}

export function drawSubGraph(options: SubGraphOptions): SubGraph {
    let xAxis = axisBottom(options.xscale)
        .tickSizeOuter(0);
    if (!options.drawTicks) {
        xAxis
            .tickFormat((d)=>"")
            .tickSize(0);
    }
    let yAxis = axisLeft(options.yscale)
        .ticks(0)
        .tickSizeOuter(0);
    let vis = options.chart.append("g")
        .attr("transform", "translate(0," + (options.n-1)*options.height + ")");
    let gx = vis.append("g")
        .attr("class", "x axis")
        .attr("transform", "translate(0," + options.height + ")")
        .call(xAxis);
    vis.append("g")
        .attr("class", "y axis")
        .call(yAxis);
    vis.append("text")
        .attr("class", "y label")
        .attr("transform", "rotate(-90)")
        .attr("y", -20)
        .attr("x", -options.height/2)
        .attr("text-anchor", "middle")
        .text(options.label);

    let overlay = vis.append("rect")
        .attr("class", "overlay")
        .attr("width", options.width)
        .attr("height", options.height);

    let pathg = vis.append("g");
    let dateLine = pathg.append("line")
        .attr("clip-path", "url(#clip)")
        .attr("x1", 0)
        .attr("y1", 0)
        .attr("x2", 0)
        .attr("y2", options.height)
        .attr("class", "dateline");
    options.draw(pathg);

    options.tooltip(vis, overlay);

    // zoom callback
    function zoomed(dx: ScaleTime<number, number>): void {
        options.zoomedRedraw(pathg, dx, overlay);
        gx.call(xAxis.scale(dx));
        setDate();
    }

    // moving dateline
    let sd: Date;
    function setDate(d?: Date): void {
        if (d == undefined) {
            d = sd;
        } else {
            sd = d;
        }
        dateLine.attr("x1", d ? options.xscale(d) : 0)
            .attr("x2", d ? options.xscale(d) : 0);
    }

    return {
        zoomStart: options.zoomStart,
        zoomEnd: options.zoomEnd,
        zoomCallback: zoomed,
        animate: options.animate,
        setDate: setDate,
    };
}
