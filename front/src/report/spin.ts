import Spinner = require("spin.js");

export function initSpinner(containerId: string): Spinner {
    let opts = {
        speed: 2,
        scale: 0.8
    }
    let target = document.getElementById(containerId);
    return new Spinner(opts).spin(target);
}
