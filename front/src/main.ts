import { select, selectAll } from "d3-selection";
import { utcParse, timeFormat } from "d3-time-format";

export { initReport } from "./report/report";

export function formatLocal(selector: string) {
    let parseTime = utcParse("%Y-%m-%d %H:%M");
    let formatTime = timeFormat("%Y-%m-%d %I:%M %p");
    selectAll(selector).text(function() {
        return formatTime(parseTime(select(this).text()));
    });
}
