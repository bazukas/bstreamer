interface Options {
    target: Element;
    interval?: number;
    spinner?: string;
    max?: number;
    stickyHeight?: number;
    callback: (eventType: string) => void;
}

declare class StayDown {
    constructor(options: Options);
}

declare module "staydown" {
    export = StayDown;
}
