var gulp = require("gulp");
var browserify = require("browserify");
var source = require('vinyl-source-stream');
var watchify = require("watchify");
var tsify = require("tsify");
var gutil = require("gulp-util");
var uglify = require('gulp-uglify');
var sourcemaps = require('gulp-sourcemaps');
var buffer = require('vinyl-buffer');

var watchedBrowserify = watchify(
  browserify({
      basedir: '.',
      entries: ['src/main.ts'],
      debug: false,
      standalone: "bs",
      cache: {},
      packageCache: {}
  })
  .plugin(tsify)
  .transform('babelify', {
      presets: ['es2015'],
      extensions: ['.ts']
  })
);

function bundle() {
  return watchedBrowserify
    .bundle()
    .on('error', function (error) { console.error(error.toString()); })
    .pipe(source('bstreamer.min.js'))
    .pipe(buffer())
    .pipe(uglify())
    .pipe(gulp.dest("../static/js/"));
}

gulp.task("default", bundle);
watchedBrowserify.on("update", bundle);
watchedBrowserify.on("log", gutil.log);
